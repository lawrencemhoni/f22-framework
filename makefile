CC = g++
LDFLAGS = -L/usr/local/lib/ -lboost_thread -lboost_system -lboost_regex -lboost_date_time -lhiredis -std=c++11 -pthread -lcrypto++  -I/usr/include/cppconn  -L/usr/lib -lmysqlcppconn

ENTRY            =  $(wildcard ./src/main.cpp)
COREHEADERS      =  $(wildcard ./src/core/*/*.hpp) 
CORESOURCES      =  $(wildcard ./src/core/*/*.cpp)
CORESOURCESINNER =  $(wildcard ./src/core/*/*/*.cpp) 

APPSOURCES =   $(wildcard ./src/app/*/*.cpp) 


main : $(CORESOURCES) $(CORESOURCESINNER) $(APPSOURCES) $(ENTRY)
	echo " Creating dependency $$@"
	$(CC)  $(CFLAGS) $? $(LDFLAGS) -o $@