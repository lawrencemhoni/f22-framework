#include <iostream>
#include <vector>
#include <string>


#include "core/bootstrap/bootstrap.hpp"
#include "registry/controllers.hpp"


using efficient::bootstrap;
using efficient::http::route;
using namespace app::controllers;


using namespace std;


int main()
{
	route _route;

	_route.get("/apps", app_controller::index)
		  .push();

	_route.post("/apps/store", app_controller::store)
	      .push();

	_route.get("/apps/permissions", app_controller::permissions)
	 	  .push();

	_route.get("/apps/types", app_controller::types)
	 	  .push();

	//It will be put
	_route.post("/apps/:id/update", app_controller::update)
		  .push();

	_route.get("/apps/:id", app_controller::view)
		  .push();


	_route.get("/organisations", organisation_controller::index)
		  .push();

	_route.post("/organisations/store", organisation_controller::store)
		  .push();

	_route.post("/organisations/:id/update", organisation_controller::update)
		  .push();

	_route.get("/organisations/:id", organisation_controller::view)
		  .push();

	_route.get("/transactions", transaction_controller::index)
		  .push();

	_route.post("/transactions/store", transaction_controller::store)
		  .push();


	_route.get("/users/accounts/types", user_controller::account_types)
		  .push();

	_route.get("/users", user_controller::index)
		  .push();

	_route.post("/users/store", user_controller::store)
		  .push();

	_route.post("/users/:id/update", user_controller::update)
		  .push();	

	_route.get("/users/:id", user_controller::view)
		  .push();

	//For now use get to be changed to post
	_route.get("/customers/create", app::controllers::customers_controller::create)
		  .push();

	_route.get("/customers", app::controllers::customers_controller::index)
		  .push();

	_route.get("/customers/:id", app::controllers::customers_controller::view)
		  .push();


	bootstrap::load();

	return 0;
}