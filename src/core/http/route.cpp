//
// route.cpp
// ~~~~~~~~~
// Copyright (c) 2016-2020 Lawrence T Mhoni (lawrencemhoni@gmail.com)
//

#include "route.hpp"


using namespace std;

using efficient::http::exceptions::route_not_found_exception;

namespace efficient {
namespace http{


vector<route> route::all ;

/**
*Create a route for a GET request.
*@param uri : the uri for the request.
*@param controller :  A function pointer where this requsted will be processed
*the controller will be passed with the request and the response.
*/
route route::get(string uri, void (*controller)(request req, response &rep) ) {
	this->uri = uri;
	this->method = "GET";
	this->controller = controller;
	generate_uri_pattern();
	reset_uri_params();
	prepare_uri_params();
	return *this;
}


/**
*Create a route for a POST request.
*@param uri : the uri for the request.
*@param controller :  A function pointer where this requsted will be processed
*the controller will be passed with the request and the response.
*/
route route::post(string uri, void (*controller)(request req, response &rep) ) {
	this->uri = uri;
	this->method = "POST";
	this->controller = controller;
	generate_uri_pattern();
	reset_uri_params();
	prepare_uri_params();

	return *this;
}


/**
*Push this route into the routes vector which is in the static property (all)
*/
void route::push(){
	
	route r;
	string exception_message;

	for(size_t i = 0; i < route::all.size(); i++) {

		r = route::all[i];

		if( r.get_pattern() == pattern ) {

			exception_message  = "The route with the uri [" + uri + "] ";
			exception_message += "Could not be created because a similar ";
			exception_message += "route was created with the uri [" + r.get_uri() + "]";
			throw http::exceptions::route_already_exists_exception(exception_message);
		}
	}

	route::all.push_back(*this);
}

/**
*Get URI
*@return string :  the private property (uri) of the route
*/
string route::get_uri() {
	return uri;
}

/**
*Get pattern
*@return string :  the private property (pattern) of the route
*/
string route::get_pattern() {
	return pattern;
}


/**
*Get method
*@return string :  the private property (method) of the route
*/
string route::get_method() {
	return method;
}


/**
*Get all routes
*@return  vector :  all routes in the all property
*/
vector<route> route::get_all() {
	return route::all;
}

/**
*Find route by uri
*@param uri: the uri string to find the route
*@throw: http::exceptions::route_not_found_exception;
*@return :  the found route
*/
route route::find_by_uri(string uri) {

	//Checking to see if the last char is a slash;
	 string last_char = uri.substr(uri.length() - 1, uri.length());

	 if(uri.length() > 1 && last_char == "/") {
	 	uri = uri.substr(0, uri.length() - 1);
	 }

	 for(size_t i = 0; i < route::all.size(); i++) {
	 	if( boost::regex_match(uri, boost::regex(route::all[i].get_pattern())) ) {
	 		return route::all[i];
	 	}	
	 }

	 throw route_not_found_exception();
}

/**
*Generate a pattern that can be matched in regex
*/
void route::generate_uri_pattern() {

	string s = pure_uri(uri);

	if(uri == "/"){
		pattern = "/";
		return;
	}

	char c = '/';
	pattern = "";

	string::size_type i = 0;
	string::size_type j = s.find(c);

	if(j == string::npos) {
		pattern = "/" + determine_pattern(s);
		return;
	}

	while (j != string::npos){
	  pattern += "/" + determine_pattern(s.substr(i, j-i));
	  i = ++j;
	  j = s.find(c, j);

	  if (j == string::npos) {
	  	 pattern += "/" + determine_pattern(s.substr(i, s.length()));
	  }  
	}
}

/**
*Removing backslashes from at the beginning and at the end 
*Making sure so that we have a proper delimeter.
*@param s : the string that needs to be purified
*/
string route::pure_uri(string s) {
	if(s.substr(0,1) == "/"){
		s = s.substr(1, s.length());
	}

	if(s.substr(s.length() - 1, 1) == "/") {
		s = s.substr(0, s.length() - 1);
	}
	return s;
}

/**
*Determine whether to return an actual item or a regular expression 
*@param s : an uri item
*/
string route::determine_pattern(string s){

	int pos  = s.find(":");
	if(pos > -1){
		return "(.*)";
	}
	return  s;
}

/**
*Prepare uri params
*Avoid duplicates in the param definition
*@throw http::exceptions::param_key_already_exsists_exception
*/
void route::prepare_uri_params(){

	vector<string> uri_items = generic::helpers::string_to_vector(pure_uri(uri), '/');

	int pos;
	string key;

	for(size_t i = 0; i <  uri_items.size();  i++) {

		pos = uri_items[i].find(":");

		if(pos > -1){
			key = uri_items[i].substr(1, uri_items[i].length());

			if (uri_params.count(key) == 0) {
				uri_params[key] = "";

			} else {

				string exception_message  = "The param key '" + key + "' ";
					   exception_message += "can not be declared more than once "; 
					   exception_message += "in the URI [" + uri + "]";

				throw http::exceptions::param_key_already_exsists_exception(exception_message);
			}
		}
	}
}

void route::reset_uri_params(){
	map<string, string> x;
	uri_params  = x;
}

/**
*Extract uri params
*@param req_uri : a string of the uri containing values
*@return map: uri_params
*/
map<string, string> route::extract_uri_params(string req_uri) {

	if(uri_params.size() > 0) {

		vector<string>  route_uri_items =  generic::helpers::string_to_vector(pure_uri(uri), '/');
		vector<string>  req_uri_items   =  generic::helpers::string_to_vector(pure_uri(req_uri), '/');

		int pos;
		string item;
		string key;

		for( size_t i = 0; i < route_uri_items.size(); i++ ) {
			item = route_uri_items[i];
			pos  = item.find(":");

			if( pos > -1 ) {
				key = item.substr(1, item.length());
				uri_params[key] = req_uri_items[i];
			}
		}
	}

	return uri_params;
}



}//http
} // namespace efficient
