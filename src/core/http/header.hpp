//
// header.hpp
// ~~~~~~~~~~
// Copyright (c) 2016-2020 Lawrence T Mhoni (lawrencemhoni@gmail.com)
//


#ifndef HTTP_SERVER3_HEADER_HPP
#define HTTP_SERVER3_HEADER_HPP

#include <string>

namespace efficient {
namespace http {

struct header
{
  std::string name;
  std::string value;
};

} // namespace http
} // efficient

#endif // HTTP_SERVER3_HEADER_HPP