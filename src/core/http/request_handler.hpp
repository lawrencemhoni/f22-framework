//
// request_handler.hpp
// ~~~~~~~~~~~~~~~~~~~
// Copyright (c) 2016-2020 Lawrence T Mhoni (lawrencemhoni@gmail.com)
//


#ifndef HTTP_SERVER3_REQUEST_HANDLER_HPP
#define HTTP_SERVER3_REQUEST_HANDLER_HPP

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <sstream>
#include <stdexcept>

#include <boost/noncopyable.hpp>
#include <boost/lexical_cast.hpp>

#include "response.hpp"
#include "request.hpp"
#include "route.hpp"
#include "../generic/helpers.hpp"
#include "../generic/log.hpp"
#include "../api/auth.hpp"
#include "mime_types.hpp"

using namespace std;

namespace efficient {
namespace http {

/// The common handler for all incoming requests.
class request_handler
  : private boost::noncopyable
{
public:
  /// Construct with a directory containing files to be served.
  explicit request_handler(const std::string& doc_root);

  /// Handle a request and produce a response.
  void handle_request(request& req, response& rep);

private:
  /// The directory containing the files to be served.
  std::string doc_root_;

  std::vector<route> routes;

  /// Perform URL-decoding on a string. Returns false if the encoding was
  /// invalid.
  static bool url_decode(const std::string& in, std::string& out);

  ///Process request parameters etc
  void process_request(request& req);

  /// extract get params in the request
  void extract_get_params(request &req);

  /// extract post params in the request
  void extract_post_params(request &req);

};


} // namespace http
} // namespace efficient

#endif // HTTP_SERVER3_REQUEST_HANDLER_HPP
