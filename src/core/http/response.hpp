//
// response.hpp
// ~~~~~~~~~
// Copyright (c) 2016-2020 Lawrence T Mhoni (lawrencemhoni@gmail.com)
//

#ifndef HTTP_SERVER3_RESPONSE_HPP
#define HTTP_SERVER3_RESPONSE_HPP

#include <iostream>
#include <string>
#include <vector>
#include <map>

#include <boost/asio.hpp>
#include <boost/lexical_cast.hpp>

#include "header.hpp"

using namespace std;

namespace efficient {
namespace http {

/// A response to be sent to a client.
class response
{
public:
  /// The status of the response.
  enum status_type
  {
    ok = 200,
    created = 201,
    accepted = 202,
    no_content = 204,
    multiple_choices = 300,
    moved_permanently = 301,
    moved_temporarily = 302,
    not_modified = 304,
    bad_request = 400,
    unauthorized = 401,
    forbidden = 403,
    not_found = 404,
    method_not_allowed = 405,
    conflict = 409,
    internal_server_error = 500,
    not_implemented = 501,
    bad_gateway = 502,
    service_unavailable = 503
  } status;

  /// The headers to be included in the response.
  std::map< std::string, std::string > headers;

  /// The content to be sent in the response.
  std::string content;

  /// Convert the response into a vector of buffers. The buffers do not own the
  /// underlying memory blocks, therefore the response object must remain valid and
  /// not be changed until the write operation has completed.
  std::vector<boost::asio::const_buffer> to_buffers();

  /// Get a stock response.
  static response stock_response(status_type status);

  //set header
  void set_header(string key, string content);

  //Send response only with content 
  void send(string new_content);

  //send response with content and status code
  void send(string new_content, response::status_type new_status_code);

  //send response with content,  status code and content type
  void send(string new_content, response::status_type new_status_code, string content_type);

};


} // namespace http
} //namespace efficient

#endif // response