//
// request.hpp
// ~~~~~~~~~~~
// Copyright (c) 2016-2020 Lawrence T Mhoni (lawrencemhoni@gmail.com)
//


#ifndef HTTP_REQUEST_HPP
#define HTTP_REQUEST_HPP

#include <string>
#include <iostream>
#include <map>
#include <vector>

#include "header.hpp"
#include "../generic/helpers.hpp"

using namespace std;
using namespace generic::helpers;

namespace efficient {
namespace http {

/// A request received from a client.
class request
{

public:

	request();

	string method;
	string uri;
	string base_uri;
	string raw_get_params;
	string decoded_uri;
	int http_version_major;
	int http_version_minor;
	map<std::string, std::string> headers;

	map<std::string, std::string> post_input;
	map<std::string, std::string> get_input;
	map<std::string, std::string> uri_params;

	string body;

	string get_get_input(string key);

	string get_post_input(string key);

	string get_body();

	string get_header(string key);

	string get_uri_param(string key);

	

private:


};

} // namespace http

} //efficient

#endif // HTTP_SERVER3_REQUEST_HPP