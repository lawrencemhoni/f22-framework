#include <iostream>
#include <stdexcept>

using namespace std;


#ifndef ROUTE_ALREADY_EXISTS_HPP
#define ROUTE_ALREADY_EXISTS_HPP

namespace efficient{
namespace http{
namespace exceptions{


	class route_already_exists_exception 
	: public runtime_error{

		public:
			route_already_exists_exception(string msg) 
			: runtime_error(msg){

			}
	};


} //exceptions
} //http
} //efficient


#endif //ROUTE_ALREADY_EXISTS_HPP