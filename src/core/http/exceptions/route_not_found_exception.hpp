#include <iostream>
#include <stdexcept>

using namespace std;



#ifndef ROUTE_NOT_FOUND_HPP
#define ROUTE_NOT_FOUND_HPP


namespace efficient {
namespace http {
namespace exceptions {


	class route_not_found_exception 
	: public runtime_error{

		public:
			route_not_found_exception() 
			: runtime_error("Route not found for the URI"){

			}
	};


} //exceptions
} //http
} //efficient

#endif