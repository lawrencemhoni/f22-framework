
#ifndef PARAM_KEY_ALREADY_EXSISTS_HPP
#define PARAM_KEY_ALREADY_EXSISTS_HPP

#include <iostream>
#include <stdexcept>

using namespace std;

namespace efficient{
namespace http{

namespace exceptions{


	class param_key_already_exsists_exception 
	: public runtime_error{

		public:
			param_key_already_exsists_exception(string msg) 
			: runtime_error(msg){

			}
	};








} //exceptions
} //http
} //efficient


#endif