//
// route.hpp
// ~~~~~~~~~
// Copyright (c) 2016-2020 Lawrence T Mhoni (lawrencemhoni@gmail.com)
//

#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <stdlib.h>
#include <cstdlib>

#include <boost/regex.hpp>

#include "request.hpp"
#include "response.hpp"
#include "exceptions/route_not_found_exception.hpp"
#include "exceptions/route_already_exists_exception.hpp"
#include "exceptions/param_key_already_exsists_exception.hpp"
#include "../generic/helpers.hpp"


#ifndef HTTP_ROUTE_HPP
#define HTTP_ROUTE_HPP

using namespace std;

namespace efficient {
namespace http{


class route{


public:

	void (*controller)(request req, response &rep);

	//Creates a GET URI
	route  get(string uri, void (*controller)( request req, response &rep ));

	//Creates a POST URI
	route  post(string uri, void (*controller)( request req, response &rep ));

	//Injecting a middleware
	route  add_middleware( bool (*middleware)());

	//return the private uri of the route
	string get_uri();

	//return the private method of the route
	string get_method();

	//return the private pattern of the route
	string get_pattern();

	//Get all routes in the static vector property
	vector<route> get_all();

	//Find the the route with url of the given name in the static vector property
	static route find_by_uri(string name);

	//Generate a uri pattern to be matched in regex
	void generate_uri_pattern();

	//Remove unnecessary delimeters (backslash in the back and front)
	string pure_uri(string s);

	//Determine if this should be treated as regex special character param or leave as is
	string determine_pattern(string s);

	//Extract URI params of a request.
	map<string, string> extract_uri_params(string uri);

	//Prepare uri_params
	void prepare_uri_params();

	void reset_uri_params();

	//Push this route to the static vector property all. with all the routes
	void push();

private:
	static vector<route> all;
	string method;
	string pattern;
	string uri;
	map<string, string> uri_params;
	
}; //route


}// http
}// efficient

#endif  //ROUTE_CLASS_HPP