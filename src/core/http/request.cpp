/*
*request.cpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#include "request.hpp"

namespace efficient {
namespace http {

request::request(){
	
}

string request::get_get_input(string key) {
	if(get_input.count(key) > 0 ) {
		return get_input[key];
	}

	return string("");
	
}
	
string request::get_post_input(string key) {
	if(post_input.count(key) > 0 ) {
		return post_input[key];
	}

	return string("");
}

string request::get_body(){
	return body;
}

string request::get_header(string key){
	key = str_to_lower(key);
	
	if(headers.count(key) > 0) {
		return headers[key];
	}
	return string("");
}

string request::get_uri_param(string key) {
	if(uri_params.count(key) > 0) {
		return uri_params[key];
	}
	return string("");
}


} // namespace http
} //efficient

