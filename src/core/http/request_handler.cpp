//
// request_handler.cpp
// ~~~~~~~~~~~~~~~~~~~
// Copyright (c) 2016-2020 Lawrence T Mhoni (lawrencemhoni@gmail.com)
//


#include "request_handler.hpp"

using namespace std;

using efficient::http::exceptions::route_not_found_exception;


namespace efficient {
namespace http {


request_handler::request_handler(const string& doc_root)
  : doc_root_(doc_root)
{
    this->routes = routes;  
}

/**
*handle request.
*@param req : request
*@param res : response
*/
void request_handler::handle_request(request& req, response& res)
{
  // Decode url to path.
  string decoded_uri;
  
  // Populate with properties
  process_request(req);

  res.set_header("Server", "F22");

  if (url_decode(req.uri, decoded_uri))
  {

    efficient::api::auth api_auth(req);

    if(api_auth.attempt()) {
      
        try {

          route _route = route::find_by_uri(req.base_uri);

          //Check if this is the correct request method for this route
          if(_route.get_method() == req.method  || _route.get_method() == "RESOURCE") {

              req.uri_params =  _route.extract_uri_params(req.base_uri);
              res.status     =  http::response::ok;

              try {

                 _route.controller(req, res); 
                  
                   res.set_header("Content-Length",
                       boost::lexical_cast<std::string>(res.content.size()));
              } catch(runtime_error e) {

                log::error(e.what());

                res.status   =  http::response::internal_server_error;
                res.content  =  "Not found.";
              }

          } else {

               res.status   =  http::response::method_not_allowed;
               res.content  =  "Method not allowed";
          }

        } catch(route_not_found_exception e) {

            res.status   =  http::response::not_found;
            res.content  =  "Not found.";
        }

    } else {
        res.status   =  http::response::unauthorized;
        res.content  =  "Not authorized.";
    }

  } 

}


/**
*Not exactly sure what this does yet.
*@param in : the original url string
*@param res : the decoded url string
*/
bool request_handler::url_decode(const string& in, string& out)
{
  out.clear();
  out.reserve(in.size());
  for (size_t i = 0; i < in.size(); ++i) {
    if (in[i] == '%') {
      if (i + 3 <= in.size()) {
        int value = 0;
        istringstream is(in.substr(i + 1, 2));
        if (is >> hex >> value) {
          out += static_cast<char>(value);
          i += 2;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }
    else if (in[i] == '+') {
      out += ' ';
    } else {
      out += in[i];
    }
  }
  return true;
}


/**
*Process the request
*@param req : the request
*/
void request_handler::process_request(request &req){

    std::vector<string> uri_vector = generic::helpers::string_to_vector(req.uri, '?');

    if(uri_vector.size() > 0){

      req.base_uri = uri_vector[0];  

      if(uri_vector.size() == 2) {
        req.raw_get_params = uri_vector[1];
      }

      extract_get_params(req);
      extract_post_params(req);
    }
}


/**
*Extract get params from the request
*Populate the params in the get_input map
*@param req : the request
*/
void request_handler::extract_get_params(request &req){

     std::vector<std::string> get_params = generic::helpers::string_to_vector(req.raw_get_params, '&');
     std::vector<std::string> params_vector;
     std::vector<std::string> final_vector;

     for(size_t i = 0; i < get_params.size();  i++){

        params_vector =  generic::helpers::string_to_vector(get_params[i], '&');

        for(size_t j = 0; j < params_vector.size(); j++){

          final_vector = generic::helpers::string_to_vector(params_vector[j], '=');

          req.get_input[ final_vector[0] ] = final_vector[1]; 
          final_vector[0]  = ""; //Pop back will cause segmentation error if final_vector[1] doesn't exist
          final_vector[1]  = "";
        }
     }
}


/**
*Extract post params from the request
*Populate the params in the post_input map
*@param req : the request
*/
void request_handler::extract_post_params(request &req) {

      if(req.method == "POST" && 
          req.headers["Content-Type"] == "application/x-www-form-urlencoded"  
          &&req.body.length() > 0){

          std::vector<string>  params_vector;
          std::vector<string>  final_vector;
       
          params_vector = generic::helpers::string_to_vector(req.body, '&');

          for(size_t i = 0; i <  params_vector.size(); i++) {

              final_vector = generic::helpers::string_to_vector(params_vector[i], '=');
              req.post_input[ final_vector[0] ] = final_vector[1]; 
              final_vector[0]  = ""; //Pop back will cause segmentation error if final_vector[1] doesn't exist
              final_vector[1]  = "";
          }
      }
}




} // namespace http
} //efficient