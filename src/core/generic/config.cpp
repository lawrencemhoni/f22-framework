/*
*config.cpp
*copyright (c) Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#include "config.hpp"

namespace generic{

vector<string> config::config_files;
string config::config_dirname;
map<string, boost::property_tree::ptree> config::resources;


/*
*Set value to the private property: config_dirname
*
*/
void config::set_config_dirname(string dirname) {
	config_dirname = dirname;
}


/*
*Load config files 
*populate the config files vector
*
*/
void config::load_config_files() {
	string pattern = "(.*).json";
	file_system::dir_list_files_regex(config_dirname, config_files, pattern, false, false);
}

/*
*Load resources 
*Populate the resources map 
*
*/
void config::load_resources() {
	if( config_files.size() == 0 ) {
		load_config_files();
	}

	for(size_t i = 0; i < config_files.size(); i++) {

			boost::property_tree::ptree pt;
	        boost::property_tree::read_json(config_files[i], pt);

			string resource = config_files[i];
			resource = boost::regex_replace(resource, boost::regex(config_dirname), "");
			resource = boost::regex_replace(resource, boost::regex("/"), "");
			resource = boost::regex_replace(resource, boost::regex(".json"), "");

			resources[resource] = pt;
	}
}

/*
*Get string (Value in the config)
*@param notation : A string notation containing resource and target in the config
*@return: string value of the json config
*
*/
string config::get_string(string notation) {

	try {

		string resource =  boost::regex_replace(notation, boost::regex("\\.(.*)"), "");
		string finder = boost::regex_replace(notation, boost::regex(resource + "\\."), "");
		return resources[resource].get<string>(finder);
	} catch (std::exception const& e) {
        std::cerr << e.what() << std::endl;
        log::error("could not get the value of the config: " + notation);
    }

	return string("");
}

/*
*Get int (Value in the config)
*@param notation : A string notation containing resource and target in the config
*@return: int value of the json config
*
*/
int config::get_int(string notation) {

	try {

		string resource =  boost::regex_replace(notation, boost::regex("\\.(.*)"), "");
		string finder = boost::regex_replace(notation, boost::regex(resource + "\\."), "");
		return resources[resource].get<int>(finder);
	} catch (std::exception const& e) {
        std::cerr << e.what() << std::endl;
        log::error("could not get the value of the config: " + notation);
    }

	return 0;
}

/*
*Derive vector from a json array in the config
*@param notation : A string notation containing resource and target in the config
*@return: populated vector
*
*/
vector<string> config::derive_vector(string notation) {

	vector<string> derived_v;

	try {

		string resource =  boost::regex_replace(notation, boost::regex("\\.(.*)"), "");
		string finder = boost::regex_replace(notation, boost::regex(resource + "\\."), "");

        BOOST_FOREACH(boost::property_tree::ptree::value_type &v, 
        	resources[resource].get_child(finder))
        {
            derived_v.push_back( v.second.data());
        }

	} catch (std::exception const& e) {
        std::cerr << e.what() << std::endl;
        log::error("could not get the array fom the config: " + notation);
    }

    return derived_v;
}

/*
*Get the ptree object
*could be used if data is too complex
*@param notation : A string notation containing resource and target in the config
*@return : ptree
*/
boost::property_tree::ptree config::get_ptree(string notation) {

	boost::property_tree::ptree pt;

	try {

		string resource =  boost::regex_replace(notation, boost::regex("\\.(.*)"), "");
		string finder;

		if(boost::regex_match(notation, boost::regex(resource + "\\.(.*)")) ) {
			finder = boost::regex_replace(notation, boost::regex(resource + "\\."), "");
		}

		if(finder.empty()) {
			pt = resources[resource];
		} else {
			pt = resources[resource].get_child(finder);
		}     	 

	} catch (std::exception const& e) {
        std::cerr << e.what() << std::endl;
        log::error("could not get the array fom the config: " + notation);
    }

    return  pt;
}


vector<string> config::get_config_files() {
	return config_files;
}







}//namespce generic

