/*
*log.hpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/

#ifndef GENERIC_LOG_HPP
#define GENERIC_LOG_HPP

#include <iostream>
#include <fstream>
#include <algorithm>
#include "calendar.hpp"
#include "file_system.hpp"

using namespace std;


namespace generic {



class log {


public:
	log();
	static void info(string msg);
	static void error(string msg);
	static void warning(string msg);

	static void set_logs_dirname(string path);

	static string get_logs_dirname();

	static string new_log_backup_name(string last_backup_file);


private:

	static string logs_dirname;
	static const int MAX_FILE_SIZE;


};


} // namespace generic


#endif //GENERIC_LOG_HPP