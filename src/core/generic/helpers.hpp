/*
*helpers.hpp
*copyright (c) Lawrence T Mhoni
*email : lawrencemhoni@gmail.com
*/
#include <iostream>
#include <vector>
#include <map>
#include <cstdlib>
#include <cstring>
#include <string>
#include <boost/algorithm/string.hpp>

using namespace std;


#ifndef GENERIC_HELPERS_HPP
#define GENERIC_HELPERS_HPP

namespace generic{

namespace helpers{

/**
*string_trim: remove occurrences of white spaces
*@param str: the string to be trimmed
*@return str: the trimmed string
*/
string string_trim(string str);

/**
*Trim string remove all the occurrences of the given character
*@param str: the string to be trimmed
*@param c : the character to be removed
*@return str: the trimmed string
*/
string string_trim(string str, char c);

/**
*Implode a vector into a string
*@param v : The vector to be joined
*@param delimeter :  The character that indicates a different vector item
*@return string: the created string from the joined vector
*/
string vector_to_string(vector<string> v, string delimeter);

/**
*Explode a string into a vector
*@param str : The string to be exploded
*@param delimeter :  The character that indicates a different vector item
*@return v: the created vector
*/
vector<string> string_to_vector(string str, char delimeter);

/**
*Explode a string into a vector 
*This is designed for strings that are created from arg tokens
*@param str : The string to be processed
*@param delimeter : The character indicating a different vector item
*@return  v: The newly created vector.
*/
vector<string> string_args_to_vector(string str, char delimeter);

/**
*Typecast a numeric string into an integer
*@param s : The numeric stringssss
*@return : The newly created vector.
*/
int string_to_int(string s);

/**
*Typecast a numeric string into an integer
*@param s : The numeric stringssss
*@return : The newly created vector.
*/
int string_to_int(string s, int default_value);

/**
*Append a vector to a vector
*@param v1: the vector to be appended to
*@param v2: the extending vector
*@return v:  the full vector
*/
//For some reason it is only working here.
template<typename T>
void extend_vector(vector<T> &v1, vector<T> v2)
{ 
  for(vector<int>::size_type i = 0; i < v2.size(); i++ ) { 
    v1.push_back(v2[i]);
  }
}

/**
*Convert an array to vector
*
*
*
*/
template<typename T1>
vector<T1> array_to_vector(int total, T1 items[]) {

  vector<T1> v;

  for(size_t i = 0; i < total; i++) {
    v.push_back(items[i]);
  }
  return v;
}

/**
*Check if vector has a value
*/
template<typename T1>
bool vector_has(vector<T1> v, T1 item) {

  cout << "items: " << v.size() << endl;
  for(size_t i = 0; i < v.size(); i++){
    if(v[i] == item) {
      return true;
    }
  }
  return false;
}


/**
*THE FOLLOWING 3 FUNCTIONS ARE TEMPORALY
*I HAVE PLACED THEM HERE BECAUSE RIGHT NOW THERE IS NO INTERNET 
*AND I DON'T KNOW THE STANDARD WAY TO IMPLEMENT THE FUNCTIONALITIES
*/

string _str_uc_first( const char * src );


/*
*Produce the string in lowercase
*@param s: original string
*@return : the string in lower case
*/
string str_to_lower(string s);


/*
*Produce the string in uppercase
*@param s: original string
*@return : the string in uppercase
*/
string str_to_upper(string s);


/*
*Produce the string with the first uppercase letter
*@param s: original string
*@return : the string with the first uppercase letter
*/
string str_uc_first(string s);


















}// helpers

}// generic

namespace std{
	//This function turns arguments into a string vector
	#define _argsv(...) generic::helpers::string_args_to_vector(#__VA_ARGS__, ',')
}

#endif //GENERIC_HELPERS_HPP

