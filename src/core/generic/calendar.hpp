/*
*calendar.hpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/

#ifndef GENERIC_CALENDAR_HPP
#define GENERIC_CALENDAR_HPP

#include <iostream>
#include <ctime>

#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/conversion.hpp>


using namespace std;
using namespace boost::gregorian;
using namespace boost::posix_time;

namespace generic {


class calendar {

public:

	//by default we set the time by local time.
	calendar();

	//construct using time_t
	calendar(time_t new_time);

	//construct using an integer value of time_t
	calendar(int new_time);

	//construct using datetime string
	calendar(string new_date_time);

	//set using time and timezone on the computer
	void set_from_local_time();

	//set using universal timezone
	void set_from_universal_time();

	//return a gregorian object
	date to_gregorian();

	//return a posix object
	ptime to_posix();

	//return a formatted string
	string string_format(string s);

private:
	ptime  _posix_time;
}; //calendar


}//namespace generic











#endif //GENERIC_CALENDAR_HPP