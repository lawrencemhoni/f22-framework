/*
*helpers.cpp
*copyright (c) Lawrence T Mhoni
*email : lawrencemhoni@gmail.com
*/


#include "helpers.hpp"

using namespace std;


namespace generic{
namespace helpers{


/**
*string_trim: remove occurrences of white spaces
*@param str: the string to be trimmed
*@return str: the trimmed string
*/
string string_trim(string str) {
  str.erase(std::remove(str.begin(),str.end(),' '),str.end());
  return str;
}

/**
*Trim string remove all the occurrences of the given character
*@param str: the string to be trimmed
*@param c : the character to be removed
*@return str: the trimmed string
*/
string string_trim(string str, char c) {
  str.erase(std::remove(str.begin(),str.end(), c ),str.end());
  return str;
}

/**
*Implode a vector into a string
*@param v : The vector to be joined
*@param delimeter :  The character that indicates a different vector item
*@return string: the created string from the joined vector
*/
string vector_to_string(vector<string> v, string delimeter) {
  return boost::algorithm::join(v, delimeter);
}

/**
*Explode a string into a vector
*@param str : The string to be exploded
*@param delimeter :  The character that indicates a different vector item
*@return v: the created vector
*/
vector<string> string_to_vector(string str, char delimeter){

  string s = str;
  vector<string> v;

  if(str == ""){
    return v;
  }

  string::size_type i = 0;
  string::size_type j = s.find(delimeter);

  if(j == string::npos){
    v.push_back(s);
    return v;
  }

  while (j != string::npos){
    v.push_back(s.substr(i, j-i));
    i = ++j;
    j = s.find(delimeter, j);

    if (j == string::npos){
       v.push_back(s.substr(i, s.length()));
    }  
  }

  return v;
}


/**
*Explode a string into a vector 
*This is designed for strings that are created from arg tokens
*@param str : The string to be processed
*@param delimeter : The character indicating a different vector item
*@return  v: The newly created vector.
*/
vector<string> string_args_to_vector(string str, char delimeter){

  string s =  string_trim( str );
         s =  string_trim( s, '\"');
       
  vector<string> v;

  if(str == ""){
  	return v;
  }

  string::size_type i = 0;
  string::size_type j = s.find(delimeter);

  if(j == string::npos){
    v.push_back(s);
    return v;
  }

  while (j != string::npos){
    v.push_back(s.substr(i, j-i));
    i = ++j;
    j = s.find(delimeter, j);

    if (j == string::npos){
       v.push_back(s.substr(i, s.length()));
    }  
  }
  return v;
}



/**
*Typecast a numeric string into an integer
*@param s : The numeric stringssss
*@return : The newly created vector.
*/
int string_to_int(string s) {
  if(s.length() > 0) {
    return stoi(s);
  }
  return 0;
}

/**
*Typecast a numeric string into an integer
*@param s : The numeric stringssss
*@return : The newly created vector.
*/
int string_to_int(string s, int default_value) {
  if(s.length() > 0) {
    return stoi(s);
  }
  return default_value;
}

/**
*THIS FUNCTION MIGHT BE PRESENT TEMPORALIY
*AND I DON'T KNOW THE STANDARD WAY TO IMPLEMENT THE FUNCTIONALITY
*/
string _str_uc_first( const char * src ) {
    int len = strlen(src);
    int i = 0;

    string new_string;

    while(i < len) {
        if(i == 0) {
            new_string += toupper(src[i]);
        } else {
            new_string += tolower(src[i]);
        }
        i++;
    }

    return new_string;
}


/*
*Produce the string in lowercase
*@param s: original string
*@return : the string in lower case
*/
string str_to_lower(string s) {
  return boost::to_lower_copy<string>(s);
}


/*
*Produce the string in uppercase
*@param s: original string
*@return : the string in uppercase
*/
string str_to_upper(string s) {
  return boost::to_upper_copy<string>(s);
}


/*
*Produce the string with the first uppercase letter
*@param s: original string
*@return : the string with the first uppercase letter
*/
string str_uc_first(string s) {
  return _str_uc_first(s.c_str());
}


}// helpers
}// generic
