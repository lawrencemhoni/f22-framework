/*
*calendar.cpp
*copyright (c) 2016-2020
*lawrencemhoni@gmail.com
*/

#include "calendar.hpp"


namespace generic {


/**
*By default time will be set from local time
*/
calendar::calendar():_posix_time(second_clock::local_time()) {

}

/**
*calendar will be set to according to the local machine
*/
calendar::calendar(string date_time)
	 	:_posix_time(time_from_string(date_time)) {

}

/**
*calendar will be set according to the timestamp
*@param new_time: the timestamp
*/
calendar::calendar(time_t new_time) {
	_posix_time = from_time_t(new_time);
}


/**
*Calendar will be set according to the timestamp in an integer value
*@param new_time :  the timestamp in an integer value
*/
calendar::calendar(int new_time) {
	_posix_time = from_time_t(new_time);
}


/**
*to posix
*@return a posix object
*/
ptime calendar::to_posix() {
	return _posix_time;
}


/**
*format to string
*@param format : the format (in ctime)
*@return formated datetime whatever.
*/
string calendar::string_format(string format_String) {

	
	struct tm * timeinfo;
	char buffer [80];

	std::time_t _time = to_time_t(_posix_time);
	time (&_time);
	timeinfo = localtime (&_time);

	strftime (buffer,80, format_String.c_str(), timeinfo);

	return buffer;
}




}// namespace generic