/*
*log.cpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/

#include "log.hpp"

namespace generic {


log::log(){


}


string log::logs_dirname;


const int log::MAX_FILE_SIZE = 200000;

/**
*Set the parent directory of log files
*@param path: directory path.
*/
void log::set_logs_dirname(string path) {
 	logs_dirname = path;
}

/**
*get the parent directory of log files
*@param path: directory path.
*/
string log::get_logs_dirname() {
	return logs_dirname;
}


/**
*Create an info log
*@param msg: the message in the log body
*/
void log::info(string msg) {

	string log_file = logs_dirname + "/info.log";
	ofstream out_log_file(log_file.c_str(), ios::app);

	int file_size = file_system::get_file_size(log_file);

	if(file_size >= MAX_FILE_SIZE) {

		vector<string> files;
		string last_backup_file;
		string new_backup_file;

		file_system::dir_list_files_regex(logs_dirname, files, "info-(.*)\\.log", false, false);
		sort(files.begin(), files.end());

		if(files.size() > 0) {

			last_backup_file = files[files.size() -1];
			new_backup_file  = new_log_backup_name(last_backup_file);

			file_system::move(log_file, new_backup_file );

			//Call this function again;
			info(msg);
			return;
		} else {
			new_backup_file = logs_dirname + "/info-1.log";
			file_system::move(log_file, new_backup_file );
		}
		
	}

	calendar c;
	out_log_file << c.string_format( "[%Y-%m-%d %H:%M:%S] ");
    out_log_file << msg;
    out_log_file << endl;

}


/**
*Create an error log
*@param msg: the message in the log body
*/
void log::error(string msg) {


	string log_file = logs_dirname + "/error.log";
	ofstream out_log_file(log_file.c_str(), ios::app);

	int file_size = file_system::get_file_size(log_file);

	if(file_size >= MAX_FILE_SIZE) {

		vector<string> files;
		string last_backup_file;
		string new_backup_file;

		file_system::dir_list_files_regex(logs_dirname, files, "error-(.*)\\.log", false, false);
		sort(files.begin(), files.end());

		if(files.size() > 0) {

			last_backup_file = files[files.size() -1];
			new_backup_file  = new_log_backup_name(last_backup_file);

			file_system::move(log_file, new_backup_file );
			//Call this function again;
			error(msg);
			return;
		} else {
			new_backup_file = logs_dirname + "/error-1.log";
			file_system::move(log_file, new_backup_file );
		}
		
	}

	calendar c;
	out_log_file << c.string_format( "[%Y-%m-%d %H:%M:%S] ");
    out_log_file << msg;
    out_log_file << endl;


}

/**
*Create a warning log
*@param msg: the message in the log body
*/

void log::warning(string msg) {

	string log_file = logs_dirname + "/warning.log";
	ofstream out_log_file(log_file.c_str(), ios::app);

	int file_size = file_system::get_file_size(log_file);

	if(file_size >= MAX_FILE_SIZE) {

		vector<string> files;
		string last_backup_file;
		string new_backup_file;

		file_system::dir_list_files_regex(logs_dirname, files, "warning-(.*)\\.log", false, false);
		sort(files.begin(), files.end());

		if(files.size() > 0) {

			last_backup_file = files[files.size() -1];
			new_backup_file  = new_log_backup_name(last_backup_file);

			file_system::move(log_file, new_backup_file );

			//Call this function again;
			warning(msg);
			return;
		} else {
			new_backup_file = logs_dirname + "/warning-1.log";
			file_system::move(log_file, new_backup_file );
		}
		
	}

	calendar c;
	out_log_file << c.string_format( "[%Y-%m-%d %H:%M:%S] ");
    out_log_file << msg;
    out_log_file << endl;
}

/**
*Create a new log backup name based on the last one
*@param last_backup_file :  name of the last backup file
*@return : name of the new backup file.
*/
string log::new_log_backup_name(string last_backup_file) {

	string old_numeric, new_numeric, new_backup_file;

	old_numeric = boost::regex_replace(last_backup_file, boost::regex("[^0-9]"), "");

	int number = std::stoi(old_numeric);

	new_numeric = std::to_string(number + 1);

	return boost::regex_replace(last_backup_file, boost::regex(old_numeric), new_numeric);

}




}// namepace generic