/*
*config.hpp
*copyright (c) Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/
#ifndef EFFICIENT_CONFIG_CONFIG_HPP
#define EFFICIENT_CONFIG_CONFIG_HPP


#include <iostream>
#include <sstream>
#include <string>
#include <map>
#include <vector>
#include <cassert>
#include <exception>
#include <boost/regex.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include "file_system.hpp"
#include "log.hpp"


using namespace std;

namespace generic{

class config {

public:

//load config files
static void load_config_files();

//set config dirname
static void set_config_dirname(string dirname);

//load resources
static void load_resources();

//get a vector with config file names
static vector<string> get_config_files();

//get a string value from json config
static string get_string(string notation);

//get an integer value from json config
static int get_int(string notation);

//derive a vector from  json array
static vector<string> derive_vector(string notation);

//get a ptree object
static boost::property_tree::ptree get_ptree(string notation);



private:

static vector<string> config_files;
static string config_dirname;
static map<string, boost::property_tree::ptree> resources;




};

}//namespce generic





#endif //EFFICIENT_CONFIG_CONFIG_HPP