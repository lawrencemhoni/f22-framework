/*
*file_system.cpp
*copyright (c) Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/

#include "file_system.hpp"

namespace generic {


/**
*Check if file exists.
*@param path: path of the file to be removed.
*@return bool .
*/
bool file_system::file_exists(string file_path) {
	
	if(!std::ifstream(file_path.c_str()) ) {
		return  false;
	}

	return true;
}

/**
*Get the size of the file in the given string
*@param file_path: the path of the file.
*@return : size of the file
*/
int file_system::get_file_size(string file_path) {
	ifstream in_file(file_path.c_str(), ios::in );
	in_file.seekg(0, ios::end);
	return in_file.tellg();
}

/**
*Touch (create an empty file)
*@param file_path :  path of the file to be created
*@return bool : true if the file was created. 
*/
bool file_system::touch(string file_path) {

	ofstream out(file_path.c_str(), ios::out);
	out.close();

	if(	std::ifstream(file_path.c_str()) ) {
		return  true;
	}
	return  false;
}


/**
*Rename or move to a new destination
*@param file_path : the current file_path
*@param new_file_path : the new file path
*@return bool:  true if the file was created.
*/
bool file_system::move(string file_path, string new_file_path) {
	bool copied = copy(file_path, new_file_path);
	bool removed = remove(file_path);

	if(copied && removed) {
		return  true;
	}

	return false;
}


/**
*copy to a new destination
*@param file_path : the current file_path
*@param new_file_path : the new file path
*@return bool:  true if the file was created.
*/
bool file_system::copy(string file_path, string new_file_path) {
	
	std::ifstream in (file_path.c_str());
	std::ofstream out (new_file_path.c_str());
	out << in.rdbuf();
	out.close();
	in.close();

	if(std::ifstream(new_file_path.c_str()) ) {
		return  true;
	}

	return false;
}


/**
*Remove a file.
*@param path: path of the file to be removed.
*@return bool if file was removed.
*/
bool file_system::remove(string file_path) {
	std::remove(file_path.c_str());

	if(!std::ifstream(file_path.c_str()) ) {
		return  true;
	}

	return  false;
}


/**
*List all items in a vector
*@param dirname : target directory
*@param v : A vector that will be populated
*@param display_dirs : Option if directories should be displayed
*@param recursive: Option if inner directories inner directories and their contents should be included
*/
void file_system::dir_list(string dirname, vector<string> &v, bool display_dirs, bool recursive) {

    DIR *dp;
	struct dirent *ep;
	string j;

	dp = opendir(dirname.c_str());

	if (dp != NULL) {
	   while ((ep = readdir(dp)) != NULL) {

	     j = ep->d_name;

	     if((ep->d_type & DT_DIR)) {

			if (strcmp (ep->d_name, "..") != 0 && strcmp (ep->d_name, ".") != 0) {

				stringstream new_dirname;

				if(dirname.substr(dirname.length() - 1,  1 ) ==  "/"  ) {
					new_dirname << dirname << ep->d_name;
				} else {
					new_dirname << dirname << "/" << ep->d_name;
				}

				if(display_dirs) {
					v.push_back(new_dirname.str());
				}

				if(recursive) {
					dir_list(new_dirname.str(), v, display_dirs, recursive);
				}
			}
	     	
	     } else {
	     		stringstream path;
	     		path << dirname  << "/" << ep->d_name;
	     		v.push_back(path.str());
	     }

	   }
	   (void) closedir (dp);
	 }
	else
	{
	// perror ("Couldn't open the directory");
	}
	// system("pause");
}


	

/**
*List all items in a vector
*@param dirname : target directory
*@param v : A vector that will be populated
*@param pattern : A regex pattern
*@param display_dirs : Option if directories should be displayed
*@param recursive: Option if inner directories inner directories and their contents should be included
*/
void file_system::dir_list_files_regex(string dirname, vector<string> &v, string pattern, 
										bool display_dirs, bool recursive) {
    DIR *dp;
	struct dirent *ep;
	string j;

	dp = opendir (dirname.c_str());
	if (dp != NULL) {
	   while ((ep = readdir (dp)) != NULL ) {

	     j = ep->d_name;

	     if((ep->d_type & DT_DIR)) {

			if (strcmp (ep->d_name, "..") != 0 && strcmp (ep->d_name, ".") != 0) {

				stringstream new_dirname;

				if(dirname.substr(dirname.length() - 1,  1 ) ==  "/"  ) {
					new_dirname << dirname << ep->d_name;
				} else {
					new_dirname << dirname << "/" << ep->d_name;
				}

				if(display_dirs) {
					v.push_back(new_dirname.str());
				}

				if(recursive) {
					dir_list_files_regex(new_dirname.str(), v, pattern, display_dirs, recursive);
				}
			}
	     	
	     } else {
	     		
	     		stringstream filename;
	     		filename << ep->d_name;

	     		if(boost::regex_match(filename.str(), boost::regex(pattern))  ) {
	     			stringstream path;
		     		path << dirname  << "/" << ep->d_name;
		     		v.push_back(path.str());

	     		}

	     }

	   }
	   (void) closedir (dp);
	 }
	else
	{
	// perror ("Couldn't open the directory");
	}
	// system("pause");
}
















}// namespace generic