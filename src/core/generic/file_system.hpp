/*
*file_system.hpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/

#ifndef GENERIC_FILE_SYSTEM_HPP
#define GENERIC_FILE_SYSTEM_HPP

#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>
#include <cstdio>
#include <ctime>
#include <cstdlib>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <cstdlib>
#include <boost/regex.hpp>

using namespace std;

namespace generic {


class file_system {


public:


file_system();

/**
*Touch (create an empty file)
*@param file_path :  path of the file to be created
*@return bool : true if the file was created. 
*/
static bool touch(string file_path);

/**
*Rename or move to a new destination
*@param file_path : the current file_path
*@param new_file_path : the new file path
*@return bool:  true if the file was created.
*/
static bool move(string file_path, string new_file_path);
	
/**
*Copy to a new destination
*@param file_path : the current file_path
*@param new_file_path : the new file path
*@return bool:  true if the file was created.
*/
static bool copy(string file_path, string new_file_path);

/**
*Remove a file.
*@param path: path of the file to be removed.
*@return bool if file was removed.
*/
static bool remove(string file_path);


/*
*Check if is dir
*@param dir_path : path of the dir
*@return bool
*/
static bool is_dir(string dir_path);

/**
*Check if file exists.
*@param path: path of the file to be removed.
*@return bool .
*/
static bool file_exists(string file_path);

/**
*List all items in a vector
*@param dirname : target directory
*@param v : A vector that will be populated
*@param display_dirs : Option if directories should be displayed
*@param recursive: Option if inner directories inner directories and their contents should be included
*/
static void dir_list(string dirname, vector<string> &v, bool display_dirs, bool recursive);


/**
*List all items in a vector
*@param dirname : target directory
*@param v : A vector that will be populated
*@param pattern : A regex pattern
*@param display_dirs : Option if directories should be displayed
*@param recursive: Option if inner directories inner directories and their contents should be included
*/
static void dir_list_files_regex(string dirname, vector<string> &v, string pattern, bool display_dirs, bool recursive);

/**
*Get the size of the file in the given string
*@param file_path: the path of the file.
*@return : size of the file
*/
static int get_file_size(string path);


private:




};  // file system


} // namespace generic




#endif // GENERIC_FILE_SYSTEM_HPP