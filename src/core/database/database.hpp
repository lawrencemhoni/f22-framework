/*
*database.hpp
*copyright (c) Lawrence T Mhoni
*email : lawrencemhoni@gmail.com
*/


#ifndef EFFICIENT_DATABASE_DATABASE_HPP
#define EFFICIENT_DATABASE_DATABASE_HPP


#include <stdlib.h>
#include <iostream>
#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
#include <cppconn/resultset_metadata.h>

#include "../generic/log.hpp"
#include "../generic/config.hpp"

using namespace generic;


#include <vector>


using namespace std;

namespace efficient {
namespace database {

class database {



public: 

	database();

	~database();

	//connect to the database
	 sql::Connection * connect();

	 void static close_connection();

	 //Set the value of the private property (database_name)
	 void set_database_name(string new_database_name);

	 database prepared_query(string query_string, vector<string> params );

	 sql::ResultSet * execute_query();

	 bool is_connected();

	 int execute_update();

private:

	string database_name;

	string database_host;

	string database_port;

	string database_user;

	string database_password;

	static sql::Connection * connection;

	sql::Driver *driver;

	sql::PreparedStatement *prepared_statement;

	sql::ResultSetMetaData *result_metadata;

	sql::ResultSet * result_set;


};//database







}//efficient
}//database

#endif //EFFICIENT_DATABASE_DATABASE_HPP
