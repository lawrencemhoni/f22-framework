/*
*database.cpp
*copyright (c) Lawrence T Mhoni
*email : lawrencemhoni@gmail.com
*/

#include "database.hpp"

namespace efficient {
namespace database {


database::database() {

	if(!is_connected()) {

		database_host = config::get_string("database.host");
		database_port = config::get_string("database.port");
		database_user = config::get_string("database.user");
		database_password = config::get_string("database.password");
		database_name = config::get_string("database.database");

		connect();
	}
}

database::~database(){
//	database::close_connection();
}

void database::close_connection() {
	connection = NULL;
}

sql::Connection * database::connection;

/*
*Check if there is a connection.
*/
bool database::is_connected() {
	if(database::connection != NULL && !database::connection->isClosed()) { 
		return true;
	}
	return false;
}


/*
*Connect to the database
*/
sql::Connection * database::connect() {
 		string url = "tcp://" + database_host + ":" + database_port;
		driver = get_driver_instance();
		database::connection = driver->connect(url, database_user, database_password);
		/* Connect to the MySQL test database */
		database::connection->setSchema(database_name);
		return database::connection;
 }


/**
*Prepare the statement
*@param query_string:  the sql string that might contain '?'
*@param params : a vector with all the real values
*@return: an object of databaase
*/
database database::prepared_query(string query_string, vector<string> params) {

	prepared_statement = database::connection->prepareStatement(query_string);

	for(size_t i = 1; i <= params.size(); i++) {
		string param = params[i - 1]; 
		prepared_statement->setString(i, param);
	}
	
	return *this;
}

/**
*Execute query
*Mainly used to return results on select
*@return : sql::ResultSet
*/
sql::ResultSet * database::execute_query() {
	result_set = prepared_statement->executeQuery();
	delete prepared_statement;
	return result_set;
}


/**
*Execute update
*Usullay used on insert, update and delete
*/
int database::execute_update() {
	int affected_rows = prepared_statement->executeUpdate();
	return affected_rows;
}






}//efficient


}//database