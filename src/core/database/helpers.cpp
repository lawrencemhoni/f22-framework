/*helpers.cpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#include "helpers.hpp"

namespace efficient {
namespace database {
namespace helpers {

query raw(string s){
	query q;
	q << s;
	return q;
}


}//namespace helpers
}//namespace database
}//namespace efficient