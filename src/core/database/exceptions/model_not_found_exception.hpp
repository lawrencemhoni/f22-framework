/*
*model_not_found_exception.hpp
*copyright(c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#ifndef MODEL_NOT_FOUND_EXCEPTION_HPP
#define MODEL_NOT_FOUND_EXCEPTION_HPP

#include <iostream>
#include <stdexcept>

using namespace std;

namespace efficient{
namespace database{
namespace exceptions{


	class model_not_found_exception 
	: public runtime_error{

		public:
			model_not_found_exception(string msg) 
			: runtime_error(msg){
			}

			model_not_found_exception() 
			: runtime_error("Model not found exception"){
			}
	};








} //exceptions
} //database
} //efficient


#endif //MODEL_NOT_FOUND_EXCEPTION