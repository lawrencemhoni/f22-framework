/*
*relation_not_found_exception.hpp
*copyright(c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#ifndef RELATION_NOT_FOUND_EXCEPTION_HPP
#define RELATION_NOT_FOUND_EXCEPTION_HPP

#include <iostream>
#include <stdexcept>

using namespace std;

namespace efficient{
namespace database{
namespace exceptions{


	class relation_not_found_exception 
	: public runtime_error{

		public:
			relation_not_found_exception(string msg) 
			: runtime_error(msg){
			}

			relation_not_found_exception() 
			: runtime_error("Relation not found exception"){
			}
	};








} //exceptions
} //database
} //efficient


#endif //RELATION_NOT_FOUND_EXCEPTION