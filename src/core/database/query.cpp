/*query.cpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#include "query.hpp"

namespace efficient {
namespace database{

query::query() {
}

query::query(string s){
	query_string += s;
}

/**
*Append string to query string on istream
*@return: object of query
*/
query & query::operator <<(string s){
	query_string += s;
	return *this;
}

/**
*Get the string value of query
*@return: query_string
*/
string query::str() {
	return query_string;
}

/**
*Reset query_string
*/
void query::reset() {
	query_string = string("");
}

/*
*Determine if this is a select statement
*@return: true or false;
*/
bool query::is_select() {

	int pos = -1;
	pos = query_string.find("SELECT"); 
	if(pos > -1) {
		return true;
	}

	pos = query_string.find("select"); 

	if(pos > -1) {
		return true;
	}

	return false;
}

}//namespace database
}//namespace efficient