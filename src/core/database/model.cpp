/*
*model.cpp
*copyright (c) Lawrence T Mhoni
*email : lawrencemhoni@gmail.com
*/

#include "model.hpp"

namespace efficient{
namespace database {

model::model(){
	total_records = 0;
	set_primary_key("id");
}

model::model(query _q):_query_builder(_q){
	total_records = 0;
	set_primary_key("id");

	if(_q.is_select()) {
		get();
	} else {
		execute_update();
	}
}

/**
*Get table name
*Get the value of the private property (table_name)
*@return:  table_name
*/
string model::get_table_name() {
	return table_name;
}

/**
*Set table name
*set the value of the private property (table_name)
*/

void model::set_table_name(string new_table_name) {
	table_name =  new_table_name;
}


/**
*Set primary key
*Set value of the private property (primary_key)
*@param  new_key_name:  the name of the primary_key
*/
void model::set_primary_key(string new_key_name) {
	primary_key = new_key_name;
}

/**
*Get primary key
*get value of the private property (primary_key)
*@param key:  the name of the primary_key
*/
string model::get_primary_key() {
	return primary_key;
}


/**
*Set foreign key
*Set value of the private property (foreign_key)
*@param key:  the name of the foreign_key
*/
void model::set_foreign_key(string new_key_name) {
	foreign_key =  new_key_name;
}



/**
*Get foreign key
*get value of the private property (foreign_key)
*@param key:  the name of the foreign_key
*/
string model::get_foreign_key() {
	return foreign_key;
}


/**
*Set foreign key value
*Set value of the private property (foreign_key value)
*@param new_value:  the value of the foreign_key
*/
void model::set_foreign_key_value(string new_value) {
	foreign_key_value =  new_value;
}

/*
*Get pivot
*@return _pivot
*/
pivot & model::get_pivot(){
	return _pivot;
}


/*
*Set pivot
*@param _p : pivot
*/
void model::set_pivot(pivot  _p){
	 _pivot = _p;
}

/**
*Insert records*
*@param data : data to be inserted
*/
void model::insert(map<string, string> data) {
	insert(data, true);
}

/**
*Insert records*
*@param data : data to be inserted
*/
model & model::insert(map<string, string> data, bool execute) {

	_query_builder.set_table_name(table_name);
	_query_builder.insert(data);

	if(execute) {
		_query_builder.build_query();
		execute_update();
	}
	
	return *this;
}

/**
*Get the first matching item in the database or create it
*@param data: fields 
*@return  : object of this model
*/
model & model::first_after_create(map<string, string> data) {
	insert(data);
	select();
	where(data);
	order_by(get_primary_key(), "DESC");
	first();

	if(rows_count() > 0 ) {
		primary_key_values.push_back(result_set->getString(primary_key));
	} 

	return 	*this;
}


/**
*Get the first matching item in the database or create it
*@param data: fields 
*@return  : object of this model
*/
model & model::first_or_create(map<string, string> data) {

	select().where(data).first();

	if(rows_count() > 0 ) {
		return *this;
	} 

	insert(data);
	select().where(data).first();

	if(rows_count() > 0 ) {
		primary_key_values.push_back(result_set->getString(primary_key));
	} 

	return 	*this;
}



/**
*Get the first item matching field values in the database or create it 
*@param data: fields 
*@param unique_fields:  fields to match against
*@return  : object of this model
*/
model & model::first_or_create(map<string, string> data, vector<string> unique_fields) {

	map<string, string> match_fields;

	for(size_t i = 0; i < unique_fields.size(); i++) {
		string key = unique_fields[i];
		match_fields[key] =  data[key];
	}

	select().where(match_fields).first();

	if(rows_count() > 0 ) {
		return *this;
	} 

	insert(data);
	select().where(match_fields).first();
	
	if(rows_count() > 0 ) {
		primary_key_values.push_back(result_set->getString(primary_key));
	} 

	return 	*this;
}

/**
*Create item or update if it the data already exists
*@param data: fields 
*@return  : object of this model
*/
model & model::create_or_update(map<string, string> data){

	select().where(data).first();

	if(rows_count() > 0 ) {
		where(data);
		update(data);
	} else {
		insert(data);
	}

	return select().where(data).first();
}

/**
*Create item or update if it the data already exists
*@param data: fields 
*@param unique_fields:  fields to match against 
*@return  : object of this model
*/
model & model::create_or_update(map<string, string> data, vector<string> unique_fields){

	map<string, string> match_fields;

	for(size_t i = 0; i < unique_fields.size(); i++) {
		string key = unique_fields[i];
		match_fields[key] =  data[key];
	}

	select().where(match_fields).first();

	if(rows_count() > 0 ) {
		model x;
		x.set_table_name(table_name);
		x.where(match_fields);
		x.update(data);
	} else {
		model x;
		x.set_table_name(table_name);
		x.insert(data);
	}

	return select().where(match_fields).first();
}

/**
*Select * records
*@return : object of this (model)
*/
model & model::select() {	
	_query_builder = _query_builder.select();
	return *this;
}

/**
*Select records
*@param fields: items to be selected
*@return : object of this (model)
*/
model & model::select(vector<string> fields) {	
	_query_builder.select(fields);
	return *this;
}

/**
*Add a cross joinning table
*@param join_table :  name of the joining table
*@return : object of this (model)
*/
model & model::cross_join(string join_table) {
	_query_builder.cross_join(join_table);
	return *this;
}


/**
*Add a left joinning table
*@param join_table :  name of the joining table
*@return : object of this (model)
*/
model & model::left_join(string join_table) {
	_query_builder.left_join(join_table);
	return *this;
}

/**
*Add a right joinning table
*@param join_table :  name of the joining table
*@return : object of this (model)
*/
model & model::right_join(string join_table) {
	_query_builder.right_join(join_table);
	return *this;
}

/**
*Add an inner joinning table
*@param join_table :  name of the joining table
*@return : object of this (model)
*/
model & model::inner_join(string join_table) {
	_query_builder.inner_join(join_table);
	return *this;
}


/**
*Add an outer joinning table
*@param join_table :  name of the joining table
*@return : object of this (model)
*/
model & model::outer_join(string join_table) {
	_query_builder.outer_join(join_table);
	return *this;
}

/**
*Add an on clause
*@param field : field_name
*@param op : the operator in the where clause
*@param value : where the field name has the value
*@return : object of this (model)
*/
model & model::on(string field, string op, string value){
	_query_builder.on(field, op, value);
	return *this;
}



/**
*Add an on clause joined by OR
*@param field : field_name
*@param op : the operator in the where clause
*@param value : where the field name has the value
*@return : object of this (model)
*/
model & model::or_on(string field, string op, string value){
	_query_builder.or_on(field, op, value);
	return *this;
}

/**
*Add an on clause joined by OR
*@param field : field_name
*@param op : the operator in the where clause
*@param value : where the field name has the value
*@return : object of this (model)
*/
model & model::on(string field, string op, string value, 
		bool is_true_value, bool is_natural, string joiner) {

	_query_builder.on(field, op, value, is_true_value, is_natural, joiner);
	return *this;
}

/**
*Add a where clause
*@param field : field_name
*@param op : the operator in the where clause
*@param value : where the field name has the value
*@return : object of this (model)
*/
model & model::where(string field, string op, string value){
	 _query_builder.where(field, op, value);
	return *this;
}

/**
*Add a where clause
*@param data : a map of all the where fields
*@return : object of this (model)
*/
model & model::where(map<string, string> data){
	_query_builder.where(data);
	return *this;
}

/**
*Add a where clause
*@param field : field_name
*@param op : the operator in the where clause
*@param value : where the field name has the value
*@return : object of this (model)
*/
model & model::where(string field, string op, string value, bool is_true_value, 
	bool is_natural, string joiner) {
	 _query_builder.where(field, op, value, is_true_value, is_natural, joiner);
	return *this;
}


/**
*Add a where clause joined by OR
*@param field : field_name
*@param op : the operator in the where clause
*@param value : where the field name has the value
*@return : object of this (model)
*/
model & model::or_where(string field, string op, string value){
    _query_builder.or_where(field, op, value);
	return *this;
}

/**
*Add a where clause joined by OR
*@param data : a map of all the where fields
*@return : object of this (model)
*/
model & model::or_where(map<string, string> data){
	_query_builder.where(data);
	return *this;
}

/**
*Add a where clause where the field is in the vector of the values
*@param field: field_name
*@param values : vector of values
*@return : an object of this model
*/
model & model::where_in(string field, vector<string> values) {
	 _query_builder.where_in(field, values);
	return *this;
}


/**
*Add a where clause where the field is in the vector of the values
*A clause joined with OR
*@param field: field_name
*@param values : vector of values
*@return : an object of this model
*/
model & model::or_where_in(string field, vector<string> values) {
	 _query_builder.or_where_in(field, values);
	return *this;
}


/**
*order by field 
*@param field :  field to order by
*@return: object of this query_builder
*/
model & model::order_by(string field) {
	_query_builder.order_by(field);
	return *this;
}

/**
*order by field 
*@param field :  field to order by
*@param order : the order to order by
*@return: object of this query_builder
*/
model & model::order_by(string field, string order) {
	_query_builder.order_by(field, order);
	return *this;
}


/**
*group by field 
*@param field :  field to order by
*@param order : the order to order by
*@return: object of this query_builder
*/
model & model::group_by(string field) {
	_query_builder.group_by(field);
	return *this;
}

/**
*Set results limit
*@param s : limit (a number)
*@return this: object of this model
*/
model & model::limit(string s) {
	_query_builder.limit(s);
	return *this;
}



/**
*Take with the following relation (eg has many)
*@param target : The name of the relation
*@return  : object of this query_builder
*/
model & model::with(string target) {

	  if(relations.count(target) == 0) {
	  	 string msg;
	  	 msg  = "Model for " + table_name + "could not take ";
	  	 msg += target +". Relation was not found.";
	  	 throw relation_not_found_exception(msg);
	  }

	  relations_to_take_with.push_back(target);
	  return *this;
}


/**
*Get results
*Execute a select * query
*@return : object of model
*/
model & model::get() {

    _query_builder.set_table_name(table_name);
	_query_builder.build_query();

	_database.prepared_query(
			 _query_builder.get_query_string(),
			 _query_builder.get_all_values());
			               
	result_set  =  _database.execute_query();

	_query_builder.reset();

	if(relations_to_take_with.size() > 0  &&   relations.empty() == 0 ) {

		while(result_set->next()) {
			primary_key_values.push_back( result_set->getString(primary_key)  );
		}
		
		reset_result_set();

		for(size_t i = 0; i < relations_to_take_with.size(); i++) {

			string key = relations_to_take_with[i];

			model * m = relations[key]->get_model();

			switch(m->relationship) {

				case m->_has_many:
					m->select();
				    m->where_in(m->get_foreign_key(), primary_key_values);
					m->get();
				break;

				case m->_belongs_to_many:
					string parent_key = m->get_pivot().get_parent_key();
					m->where_in(parent_key, primary_key_values);
					m->get();
				break;
			}

		}		
	}

	return *this;
}

/**
*Get the first item in the database
*@return  : object of this query_builder
*/
model & model::first() {

	_query_builder.limit("1");
    get();

    if(has_next()) {
    	primary_key_values.push_back(result_set->getString(primary_key));
    }

	return *this;
}

/**
*Get the first item in the database or throw exception if doest exist
*@return  : object of query_builder
*/
model & model::first_or_fail() {

	_query_builder.limit("1");
    get();

	if(has_next() && rows_count() > 0){
		primary_key_values.push_back(result_set->getString(primary_key));
		return *this;
	}

	string msg = "No result was found for the query";
	throw model_not_found_exception(msg);
}

/**
*Execute every query that updates the database
*/
model & model::execute_update() {
	_database.prepared_query(_query_builder.get_query_string(), _query_builder.get_all_values())
			 .execute_update();
    _query_builder.reset();
}

/**
*Get paginated results
*@param items_per_page : number of items per page
*@return: object of this model
*/
model & model::paginate(int items_per_page) {
	paginate(items_per_page, 1);
	return *this;
}

/**
*Get paginated results
*@param items_per_page : number of items per page
*@param page_number : default page number that sets offset
*@return: object of model
*/
model & model::paginate(int items_per_page, int page_number) {

	_query_builder.set_table_name(table_name);
	query_builder temp_query_builder;
	temp_query_builder = _query_builder;
	temp_query_builder.build_query();

	page_number = (page_number <= 0)? 1 : page_number;
		  
	_database.prepared_query(
			  temp_query_builder.get_query_string(),
			  temp_query_builder.get_all_values());

	result_set  =  _database.execute_query();

	page_number = (page_number <= 0)? 1 : page_number;

    _query_builder.paginate(items_per_page, page_number, result_set->rowsCount() );

   return get();
}

/**
*Rows count
*@return: no of rows
*/
int model::rows_count() {
	return result_set->rowsCount();
}

/**
*Total records when there is pagination
*@return: no of records
*/
int model::get_total_records() {
	if(total_records == 0) {
		return result_set->rowsCount();
	}

	return rows_count();
}


/**
*children of the row
*@param name: Name of the many 
*@return: object of model
*/
void model::children(string key, model &m) {
	 string parent_key_value;
	 parent_key_value = result_set->getString(primary_key); 
	 children(key, parent_key_value, m);
}


/**
*children of the row
*@param name: Name of the many 
*@param new_key_value :  The primary key of the foreign table
*@return: object of model
*/
void model::children(string key, string parent_key_value, model &m) {

	 m = *relations[key]->get_model();

	 switch(m.relationship) {
	 	case m._has_many:
	 	has_many_children(key, parent_key_value, m );
	 	break;
	 	case m._belongs_to_many:
	 	belongs_to_many_children(key, parent_key_value, m);
	 	break;
	 }
}


/**
*populate has many children to the model filter by given value
*@param name: Name of the many 
*@param new_foreign_key_value :  The primary key of the foreign table
*@return: object of model
*/
void model::has_many_children(string key, string new_foreign_key_value, model &m) {
	m = *relations[key]->get_model();
	m.set_foreign_key_value(new_foreign_key_value);
	m.reset_result_set();
}


/**
*populate belongs to many children to the model filter by given value
*@param key: Name of the relation 
*@param new_pivot_parent_value :  The parent key  alue of the pivot table
*@param &m : model to be populated with children data.
*@return: object of model
*/
void model::belongs_to_many_children(string key, string new_pivot_parent_value, model &m) {
	 m = *relations[key]->get_model();
	 m.get_pivot().set_parent_key_value(new_pivot_parent_value);
	 m.reset_result_set();
}

/**
*Attach related children (Many to many relations)
*@param key : key of the relation
*@param child_values (values of the children)
*@return : object of model
*/
model & model::attach(string key, vector<string> child_values) {

	if(relations.count(key) == 0) {
	  	 string msg;
	  	 msg  = "Model for " + table_name + " could not attach ";
	  	 msg +=  key +". Relation was not found.";
	  	 throw relation_not_found_exception(msg);
	}

	model * child_model = relations[key]->get_model();

	switch(child_model->relationship) {
		case child_model->_belongs_to_many:
			pivot p = child_model->get_pivot();

			string child_key  = p.get_child_key();
			string parent_key = p.get_parent_key();
			string pivot_table_name = p.get_table_name();

			model pivot_model;
			pivot_model.set_table_name(pivot_table_name);

			for(size_t i = 0; i < primary_key_values.size();  i++) {
				for(size_t j = 0; j < child_values.size(); j++) {

					map<string, string> data;
					data[parent_key] =   primary_key_values[i];
					data[child_key]  =   child_values[j];
					pivot_model.insert(data, false);
				}
			}

			pivot_model.build();
			pivot_model.execute_update();
		break;
	}

	return *this;
}

/**
*Detach related children (Many to many relations)
*@param key : key of the relation
*@return : object of model
*/
model & model::detach(string key) {

	if(relations.count(key) == 0) {
	  	 string msg;
	  	 msg  = "Model for " + table_name + " could not detach ";
	  	 msg +=  key +". Relation was not found.";
	  	 throw relation_not_found_exception(msg);
	}

	model * child_model = relations[key]->get_model();

	switch(child_model->relationship)
	{
		case child_model->_has_many:
		{

			string fk = child_model->get_foreign_key();
			 model x = * child_model;

			 x.where_in(fk, primary_key_values)
			  .remove();
			break;		
		}

		case child_model->_belongs_to_many:
		{
			pivot p = child_model->get_pivot();
			string parent_key = p.get_parent_key();
			string pivot_table_name = p.get_table_name();

			model pivot_model;
			pivot_model.set_table_name(pivot_table_name);
			pivot_model.where_in(parent_key, primary_key_values)
			 		   .remove();
			break;
		}
		default:

			break;
	}
	return *this;
}

/**
*Check if result_set has next.
*@return bool
*/
bool model::has_next() {

	switch(relationship) {

		case _has_many:
			while(result_set->next()) {
				if(result_set->getString(foreign_key) == foreign_key_value ) {
					result_set->previous();
					break;
				}
			}
			return result_set->next();
		break;

		case _belongs_to_many:

			while(result_set->next()) {

				string key = _pivot.get_parent_key();

					key = boost::regex_replace(key, boost::regex("(.*)\\."), "");

				if(result_set->getString(key) == _pivot.get_parent_key_value()) {
					result_set->previous();
					break;
				}
			}
			return result_set->next();
		break;

		default : 
			return result_set->next();
		break;

	}	
}

/**
*Get string value for a field
*@param field_name : name of the field.
*@return : row field value of the field_name
*/
string model::operator[](string field_name){
	if(rows_count() > 0) {
		return result_set->getString(field_name);
	}

	return "";
}

/**
*Add has many to the relations map
*/
void model::add_has_many(string key, model * m, string fk) {
	relations[key] = new efficient::database::relations::has_many(m, fk);
}

/**
*Add belongs to many to the relations map
*@param key : name of the children
*@param pivot_name : name of the pivot table
*@param pivot_parent_field : field name of the parent table in the pivot table
*@param pivot_child_field: field name of the child table in the pivot table
*/
void model::add_belongs_to_many(string key, model * child, string pivot_name,
		 	string pivot_parent_field, string pivot_child_field) {
	pivot p;
	p.set_table_name(pivot_name);
	p.set_parent_key(pivot_parent_field);
	p.set_child_key(pivot_child_field);

	child->set_pivot(p);

	relations[key] = new efficient::database::relations::belongs_to_many(this, child);
}

/**
*Execute an update
*@param data: a map with field and value to update in the database
*@return : object of this model
*/

model & model::update(map<string, string> data) {
	_query_builder.set_table_name(table_name);
	_query_builder.update(data).build_query();
	execute_update();
	return *this;
}

/**
*Execute a delete statement
*/
void model::remove() {
	_query_builder.set_table_name(table_name);
	_query_builder.remove().build_query();
	execute_update();
}

/**
*Reset result set
*/
void model::reset_result_set() {
	result_set->first();
	result_set->previous();
}

void model::build_query() {
	_query_builder.set_table_name(table_name);
	_query_builder.build_query();
}

void model::build() {
	_query_builder.set_table_name(table_name);
	_query_builder.build_query();
}

/**
*Build query string and show
*@return : query string
*/
string model::show_query_string() {
	build_query();

	return _query_builder.get_query_string();
}


sql::ResultSet *  model::get_result_set() {
	return result_set;
}


}//namespace database
}//namespace efficient