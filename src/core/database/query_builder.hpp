/*
*query_builder.hpp
*copyright (c) Lawrence T Mhoni
*email : lawrencemhoni@gmail.com
*/

#ifndef EFFICIENT_DATABASE_QUERY_BUILDER_HTTP 
#define EFFICIENT_DATABASE_QUERY_BUILDER_HTTP 


#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <utility>
#include <sstream>

#include "query.hpp"
#include "pagination.hpp"
#include "../generic/helpers.hpp"

using namespace std;
using namespace generic::helpers;

using efficient::database::pagination;

namespace sql{

#define _list(...) generic::helpers::args_to_vector(__VA_ARGS__)


} //sql


namespace efficient{
namespace database{

class query_builder {

public: 

	query_builder();
	
	query_builder(query q);

	//new table_name can set on instantiation
	query_builder(string new_table_name);

	//return the value of the private property (table_name)
	string get_table_name();

	//set the value of the private property  (table_name)
	void set_table_name(string new_table_name);

	//insert
	query_builder & insert(map<string, string> data);

	//set select fields
	query_builder & select(vector<string> fields);

	//select *
	query_builder & select();

	//set table name for select statement)
	query_builder & from(string);

	//push_back left join to joins
	query_builder & left_join(string join_table);

	//push_back right join to joins
	query_builder & right_join(string join_table);

	//push_back inner_join to joins
	query_builder & inner_join(string join_table);

	//push_back outer_joins to joins
	query_builder & outer_join(string join_table);

	//push_back cross_join to joins
	query_builder & cross_join(string join_table);

	query_builder & cross_join(string field,  string op, string value);

	//update
	query_builder & update(map<string, string> data);

	//delete
	query_builder & remove();

	//Where clause in the sql statement
	query_builder & where(string field, string op, string value);

	//Where clause in the sql statement
	query_builder & where(map<string, string> data);

	//Where clause in the sql statement
	query_builder & where(string field, string op, string value, bool is_true_value, bool is_natural, string joiner);

	//or where.
	query_builder & or_where(string field, string op, string value);

	//Where clause in the sql statement
	query_builder & or_where(map<string, string> data);

	//Where in
	query_builder & where_in(string field, vector<string> values);

	//Where in
	query_builder & or_where_in(string field, vector<string> values);

	//on clause in the sql statement
	query_builder & on(string field, string op, string value);

	//on clause in the sql statement
	query_builder & on(string field, string op, string value, 
	bool is_true_value, bool is_natural, string joiner);

	//or on
	query_builder & or_on(string field, string op, string value);


	//order by field in a default order
	query_builder & order_by(string field);

	//order by field in a given order
	query_builder & order_by(string field, string order);

	//group by field
	query_builder & group_by(string field);

	//limit
	query_builder & limit(string s);

	//set pagination
	query_builder & paginate(int items_per_page);

	//set pagination with a page number
	query_builder & paginate(int items_per_page, int page_number);

	//set pagination with a page number and total results
	query_builder & paginate(int items_per_page, int page_number, int total_items);

	//Join all the joins into one
	string build_joins();

	//build on clauses from where clause chunks
	string build_on_clauses();

	//build where clauses from where clause chunks
	string build_where_clauses();


	//build insert query string
	void build_insert_query();

	//build select query string
	void build_select_query();

	//build update query string
	void build_update_query();

	//build delete query string
	void build_remove_query();

	string build_group_by();

	//Build the order by part
	string build_order_by();

	//Build the limit part
	string build_limit();

	//build the offset part.
	string build_offset();

	//build query string
	query_builder & build_query();

	//set query
	void set_query(query q);

	//Return the value of the private property (query_string)
	string get_query_string();

	//Return the value of the private property (all_values)
	vector<string> get_all_values();

	//Reset everything
	void reset();

private: 

	string table_name;

	//The string containing the SQL
	query _query;

	//Type of the query

	enum {
		_insert,
		_select,
		_update,
		_remove,
		_raw
	} query_type;


	vector<string> select_fields;

	//joins eg inner join etc
	vector<string> joins;

	//Where clauses with the clause and the type of joining
	vector< pair< string, string> > where_clause_chunks;

	//on clauses with the clause and the type of joining
	vector< pair< string, string> > on_clause_chunks;


	vector< pair<string, string> > order_by_chunks;

	vector <string> group_by_fields;

	//limit
	string results_limit;

	//offset
	string results_offset;

	pagination _pagination;


	//Fields with data to be inserted
	vector < map<string, string> > insert_data;

	//Fields with data to be updated
	map<string, string> update_data;

	vector<string> insert_values;
	vector<string> update_values;
	vector<string> where_clause_values;
	vector<string> on_clause_values;
	vector<string> all_values;

};



} //efficient
} // database


#endif // EFFICIENT_DATABASE_QUERY_BUILDER_HTTP 