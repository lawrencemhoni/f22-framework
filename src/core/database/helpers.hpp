/*helpers.hpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/
#ifndef EFFICIENT_DATABASE_HELPERS_HPP
#define EFFICIENT_DATABASE_HELPERS_HPP

#include "query.hpp"

using namespace efficient::database;

namespace efficient {
namespace database {
namespace helpers {

query raw(string s);


}//namespace helpers
}//namespace database
}//database efficient


#endif // EFFICIENT_DATABASE_HELPERS_HPP