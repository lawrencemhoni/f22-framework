/*
*has_many.hpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#ifndef EFFICIENT_DATABASE_RELATIONS_HAS_MANY_HPP
#define EFFICIENT_DATABASE_RELATIONS_HAS_MANY_HPP

#include <iostream>
#include "relation.hpp"

using namespace std;

namespace efficient {
namespace database {
namespace relations {

class has_many : public relation {

public:
	has_many(model * _m, string fk);

};

}//namespace relations
}//namespace database
}//namespace efficient

#endif //EFFICIENT_DATABASE_RELATIONS_HAS_MANY_HPP