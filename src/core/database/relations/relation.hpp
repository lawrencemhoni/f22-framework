/*
*relation.hpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#ifndef EFFICIENT_DATABASE_RELATIONS_RELATION_HPP
#define EFFICIENT_DATABASE_RELATIONS_RELATION_HPP

namespace efficient {
namespace database {

	class model;

}
}

using efficient::database::model;

namespace efficient{
namespace database {
namespace relations{

class relation {
public:
	relation();
	void set_model(model * _m);
	model * get_model();

protected:
	model * _model;


};

}  //namespace relations
} //namespace database
} //namespace efficient


#endif //EFFICIENT_DATABASE_RELATIONS_RELATION_HPP