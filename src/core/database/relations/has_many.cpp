/*
*has_many.cpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#include "has_many.hpp"
#include  "../model.hpp"

namespace efficient {
namespace database {
namespace relations {


has_many::has_many(model * _m, string fk){
	_m->set_foreign_key(fk);
	_m->relationship = _m->_has_many;
	_model = _m;
}


}//namespace relations
}//namespace database
}//namespace efficient
