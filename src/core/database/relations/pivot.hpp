/*pivot.hpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#ifndef EFFICIENT_DATABASE_RELATIONS_PIVOT_HPP
#define EFFICIENT_DATABASE_RELATIONS_PIVOT_HPP
	
#include <iostream>

using namespace std;

namespace efficient{
namespace database{
namespace relations{

class pivot{
public:
	pivot();
	void set_table_name(string new_table_name);
	void set_parent_key(string new_parent_key);
	void set_child_key(string new_child_key);
	void set_parent_key_value(string new_parent_key_value);
	void set_child_key_value(string new_child_key_value);

	string get_table_name();
	string get_parent_key();
	string get_child_key();
	string get_parent_key_value();
	string get_child_key_value();


private:
	
	string table_name;
	string parent_key;
	string child_key;
	string parent_key_value;
	string child_key_value;
};

}//namespace relations
}//namespace database
}//namespace efficient



#endif //EFFICIENT_DATABASE_RELATIONS_PIVOT_HPP