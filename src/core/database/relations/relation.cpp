/*
*relation.cpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#include "../model.hpp"

using efficient::database::model;

namespace efficient{
namespace database {
namespace relations{

relation::relation() {

}

void relation::set_model(model * _m) {
	_model = _m;
}

model * relation::get_model() {
	return _model;
}





}  //namespace relations
} //namespace database
} //namespace efficient

