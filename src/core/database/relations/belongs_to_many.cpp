/*
*belongs_to_many.cpp
*copyright (c) 2016 - 2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/


#include "belongs_to_many.hpp"
#include "../model.hpp"


namespace efficient{
namespace database {
namespace relations{

belongs_to_many::belongs_to_many(model * _parent, model * _child) {

	parent = _parent;
	_model  = _child;

	_model->relationship = _model->_belongs_to_many;

	pivot p = _model->get_pivot();
	pivot_table_name = p.get_table_name();
	pivot_parent_key = pivot_table_name + "." + p.get_parent_key();
	pivot_child_key  = pivot_table_name + "." + p.get_child_key();

	construct_model();
}

string belongs_to_many::get_parent_table_name() {
	return parent->get_table_name();
}

string belongs_to_many::get_parent_primary_key() {
	return parent->get_table_name() + "."+ parent->get_primary_key();
}

string belongs_to_many::get_child_table_name() {
	return _model->get_table_name();
}

string belongs_to_many::get_child_primary_key() {
	return _model->get_table_name() + "." + _model->get_primary_key();
}

void belongs_to_many::construct_model() {
	string parent_table_name  = get_parent_table_name();
	string child_primary_key  = get_child_primary_key();
	string parent_primary_key = get_parent_primary_key();

	vector <string>  select_fields;

	 select_fields.push_back(get_child_table_name() + ".*");
	 select_fields.push_back(pivot_parent_key);

	_model->select(select_fields);
	_model->inner_join(parent_table_name);
	_model->cross_join(pivot_table_name);
	_model->on( pivot_child_key, "=",  child_primary_key, false, true, "AND" );
	_model->where( pivot_parent_key, "=", parent_primary_key, false, true, "AND");
}


}//namespace relations
}//namespace database
}//namespace efficient

