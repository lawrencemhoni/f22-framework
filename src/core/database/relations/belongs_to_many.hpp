/*
*belongs_to_many.hpp
*copyright (c) 2016 - 2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#ifndef EFFICIENT_DATABASE_RELATIONS_BELONGS_TO_MANY
#define EFFICIENT_DATABASE_RELATIONS_BELONGS_TO_MANY

#include <iostream>
#include <vector>
#include "relation.hpp"
#include "pivot.hpp"
#include "../../generic/helpers.hpp"


using namespace std;
using namespace generic;


namespace efficient{
namespace database {
namespace relations{

class belongs_to_many : public relation {

public:
	belongs_to_many(model * _parent, model * _child);

	string get_parent_table_name();

	string get_parent_primary_key();

	string get_child_table_name();

	string get_child_primary_key();

private:
	model * parent;
	string pivot_table_name;
	string pivot_parent_key;
	string pivot_child_key;

	void construct_model();

};

}//namespace relations
}//namespace database
}//namespace efficient


#endif //EFFICIENT_DATABASE_RELATIONS_BELONGS_TO_MANY