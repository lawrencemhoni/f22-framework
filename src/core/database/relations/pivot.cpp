/*pivot.cpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/
#include "pivot.hpp"

using namespace std;

namespace efficient{
namespace database{
namespace relations{

pivot::pivot() {

}

void pivot::set_table_name(string new_table_name) {
	table_name = new_table_name;
}

void pivot::set_parent_key(string new_parent_key) {
	parent_key = new_parent_key;
}


void pivot::set_child_key(string new_child_key) {
	child_key = new_child_key;
}

void pivot::set_parent_key_value(string new_parent_key_value) {
	parent_key_value = new_parent_key_value;
}

void pivot::set_child_key_value(string new_child_key_value){
	child_key_value = new_child_key_value;
}

string pivot::get_table_name() {
	return table_name;
}

string pivot::get_parent_key() {
	return parent_key;
}

string pivot::get_child_key() {
	return child_key;
}

string pivot::get_parent_key_value() {
	return parent_key_value;
}

string pivot::get_child_key_value() {
	return child_key_value;
}


}//namespace relations
}//namespace database
}//namespace efficient


