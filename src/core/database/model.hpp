/*
*model.hpp
*copyright (c) Lawrence T Mhoni
*email : lawrencemhoni@gmail.com
*/

#ifndef EFFICIENT_DATABASE_MODEL_HPP
#define EFFICIENT_DATABASE_MODEL_HPP

#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <boost/regex.hpp>

#include "database.hpp"
#include "query.hpp"
#include "query_builder.hpp"
#include "helpers.hpp"
#include "relations/pivot.hpp"
#include "relations/relation.hpp"
#include "relations/has_many.hpp"
#include "relations/belongs_to_many.hpp"
#include "exceptions/model_not_found_exception.hpp"
#include "exceptions/relation_not_found_exception.hpp"

#include "../generic/helpers.hpp"

using namespace efficient::database::exceptions;
using namespace efficient::database::relations;
using namespace generic::helpers;

namespace db = efficient::database::helpers;

namespace efficient {
namespace database {

class model {

public:

	model();

	model(query _q);

	~model(){
		if(result_set != NULL) {
			result_set = NULL;
		}
	}

	enum {
		_does_not_belong_to_any,
		_has_many,
		_belongs_to_many
	} relationship;

	//get table name
	string get_table_name();

	void set_table_name(string new_table_name);

	//set primary_key
	void set_primary_key(string new_key_name);

	//Get the primary key
	string get_primary_key();

	//set foreign_key 
	void set_foreign_key(string new_key_name);

	//Get the foreign key
	string get_foreign_key();

	//set parent foreign key value
	void set_foreign_key_value(string new_foreign_key_value);

	//Get pivot object
	pivot & get_pivot();

	//Set pivot object
	void set_pivot(pivot _p);

	//perform insert
	void insert(map<string, string> data);
	
	model & insert(map<string, string> data, bool execute);

	model & first_after_create(map<string, string> data);

	//perform insert
	model & first_or_create(map<string, string> data);

	//perform insert
	model & first_or_create(map<string, string> data, vector<string> match_fields);

	//perform insert
	model & last_or_create(map<string, string> data);

	//create or update
	model & create_or_update(map<string, string> data);

	//create or update matching specific fields
	model & create_or_update(map<string, string> data, vector<string> match_fields);

	//perform select
	model & select();
	
	//perform select
	model & select(vector<string> fields);

	//add a cross joining table
	model & cross_join(string join_table);

	//add a left joining table
	model & left_join(string join_table);

	//add a right joining table
	model & right_join(string join_table);

	//add an inner joining table
	model & inner_join(string join_table);

	//add an outer joining table
	model & outer_join(string join_table);

	//many to many (Take with the following on of the many)
	model & with(string target );

	//on clause
	model & on(string field, string op, string value);

	//on clause
	model & on(string field, string op, string value, 
		bool is_true_value, bool is_natural, string joiner);
	
	//on joined by OR
	model & or_on(string field, string op, string value);

	//where clause
	model & where(string field, string op, string value);	

	//where clause
	model & where(map<string, string> data);

	model & where(string field, string op, string value, bool is_true_value, 
	bool is_natural, string joiner);

	//Where joined by OR
	model & or_where(string field, string op, string value);

	//Where joined by OR
	model & or_where(map<string, string> data);

	//where field values in the vector
	model & where_in(string field, vector<string> values);

	//where field values in the vector a clause joined by OR
	model & or_where_in(string field, vector<string> values);

	//order by field in a default order
	model & order_by(string field);

	//order by field in a given order
	model & order_by(string field, string order);

	model & group_by(string field);

	//limit
	model & limit(string s);

	//perform update
	model & update(map<string, string> data);

	//perform delete
	void remove();

	//get results 
	model & get();

	//get only the first result
	model & first();

	//get only the first result or throw an exception
	model & first_or_fail();

	//Execute query that makes updates to the database
	model & execute_update();

	//get paginated results
	model & paginate(int items_per_page);

	//get paginated results
	model & paginate(int items_per_page, int page_number);

	//count rows
	int rows_count();

	//Total records when paginated.
	int get_total_records();

	//populate children to the model filter by primary key
	void children(string key,model &m);
	
	//populate children to the model filter by given value
	void children(string key, string parent_key_value, model &m);

	//populate has many children to the model filter by given value
	void has_many_children(string target, string new_foreign_key_value, model &m);

	//populate belongs to many children to the model filter by given value
	void belongs_to_many_children(string target, string new_pivot_parent_value, model &m);

	//check if result set has next;
	bool has_next();

	//Retrievning a field value from a result
	string operator[](string field_name);

	//add a has many model
	void add_has_many(string model_name, model * m, string fk);

	//add a has many model
	void add_belongs_to_many(string key, model * child, string pivot_name,
		 	string pivot_parent_field, string pivot_child_field);

	//Attach children (Many to many relations)
	model & attach(string key, vector<string> child_values);

	//Detach children (Many to many relations)
	model & detach(string key);

	// reset result set
	void reset_result_set();


	string show_query_string();

	void build_query();
	
	void build();

	sql::ResultSet *  get_result_set();


private:

	string table_name;
	query_builder _query_builder;
	database _database;
	pivot  _pivot;

	sql::ResultSet * result_set;

	map < string, relation *>  relations;

	//Many (many to many) to be taken with
	vector<string> relations_to_take_with;
	vector<string> primary_key_values;

	string primary_key;
	string foreign_key;
	string foreign_key_value;

	int total_records;



}; //model

}//efficient

}//database

#endif //EFFICIENT_DATABASE_MODEL_HPP
