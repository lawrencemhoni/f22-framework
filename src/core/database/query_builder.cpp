/*
*query_builder.cpp
*copyright (c) Lawrence T Mhoni
*email : lawrencemhoni@gmail.com
*/
#include "query_builder.hpp"

namespace efficient {
namespace database {

query_builder::query_builder() {
}

//Set table name on instantiation
query_builder::query_builder(string new_table_name) {
	table_name = new_table_name;
}

//Set table name on instantiation
query_builder::query_builder(query q) {

	query_type = _raw;
	_query = q;
}

/**
*get the value of the private property (table_name)
*@return string: table_name
*/
string query_builder::get_table_name() {
	return table_name;
}

/**
*set value to the private property (table_name)
*@param new_table_name : the new value
*/
void query_builder::set_table_name(string new_table_name) {
	table_name = new_table_name;
}

/**
*Insert
*@param data: data to be inserted
*@return : object of query_builder
*/
query_builder & query_builder::insert(map<string,string> data) {
	query_type  = _insert;
	insert_data.push_back(data);
	return *this;
}

/**set select fields
*@param fields: (All the fields the select statement will have)
*@return : object of query_builder
*/
query_builder & query_builder::select(vector<string> fields) {
	query_type    = _select;
	extend_vector(select_fields, fields);
	//select_fields = fields;
	return *this;
}

/**set select fields
*select *
*@return : object of query_builder
*/
query_builder & query_builder::select() {
	query_type = _select;
	return *this;
}


/**
*from
*@param new_table_name (table_name)
*@return : object of query_builder
*/
query_builder & query_builder::from(string new_table_name) {
	table_name = new_table_name;
	return *this;
}

/**
*push_back inner join to joins
*@param join_table : the inner joinning table name
*@return : object of query_builder
*/
query_builder & query_builder::inner_join(string join_table) {
	joins.push_back("INNER JOIN " + join_table);
	return *this;
}


/**
*push_back out_join to joins
*@param join_table : outer oining table name
*@return : object of query_builder
*/
query_builder & query_builder::outer_join(string join_table) {
	joins.push_back("OUTER JOIN " + join_table);
	return *this;
}


/**
*push_back left_join to joins
*@param join_table : the left joinning table name
*@return : object of query_builder
*/
query_builder & query_builder::left_join(string join_table) {
	joins.push_back("LEFT JOIN " + join_table);
	return *this;
}

/**
*push_back right join to joins
*@param join_table : the right joinning table name
*@return : object of query_builder
*/
query_builder & query_builder::right_join(string join_table) {
	joins.push_back("RIGHT JOIN " + join_table);
	return *this;
}


/**
*push_back cross_join to joins
*@param join_table : the cross joinning table name
*@return : object of this (query_builder)
*/ 
query_builder & query_builder::cross_join(string join_table) {
	joins.push_back("CROSS JOIN " + join_table);
	return *this;
}

/**
*UPDATE
*@param data : data to be inserted
*@return : object of this (query_builder)
*/
query_builder & query_builder::update(map<string, string> data) {
	query_type = _update;
	update_data = data;
	return *this;
}

/**
*DELETE
*@return : object of this (query_builder)
*/
query_builder & query_builder::remove() {
	query_type = _remove;
	return *this;
}

/**
*Add on clause
*@param field : field_name
*@param op : the operator in the where clause
*@param value : where the field name has the value
*@return : object of this (query_builder)
*/
query_builder & query_builder::on(string field, string op, string value) {
	return on(field,op,value, true, false, "AND");
}

/**
*Add on clause
*@param field : field_name
*@param op : the operator in the where clause
*@param value : where the field name has the value
*@return : object of this (query_builder)
*/
query_builder & query_builder::on(string field, string op, string value, 
	bool is_true_value, bool is_natural, string joiner) {

	string clause;
	joiner = (joiner == "OR")? "OR" : "AND";

	if(is_true_value && !is_natural){
		clause = field + "=" +"?";
		on_clause_values.push_back(value);
	} else if(!is_natural) {
		clause = field + "=" +"\"" + value + "\"";
	} else {
		clause = field + "="  + value;
	}

	pair<string, string> on_pair(joiner, clause);
	on_clause_chunks.push_back(on_pair);
	
	return *this;
}

/**
*Add a on clause joined by OR
*@param field : field_name
*@param op : the operator in the where clause
*@param value : where the field name has the value
*@return : object of this (query_builder)
*/
query_builder & query_builder::or_on(string field, string op, string value) {
	return on(field,op,value, true, false, "OR");
}

/**
*Add a where clause
*@param field : field_name
*@param op : the operator in the where clause
*@param value : where the field name has the value
*@return : object of this (query_builder)
*/
query_builder & query_builder::where(string field, string op, string value, 
	bool is_true_value, bool is_natural, string joiner) {
	
	joiner = (joiner == "OR")? "OR" : "AND";
	string clause;

	if(is_true_value && !is_natural){
		clause = field + "=" +"?";
		where_clause_values.push_back(value); 
	} else if(!is_natural) {
		clause = field + "=" +"\"" + value + "\"";
	} else {
		clause = field + "="  + value;
	}
	
	pair<string, string> where_pair(joiner, clause);
	where_clause_chunks.push_back(where_pair);
	
	return *this;
}

/**
*Add a where clause
*@param field : field_name
*@param op : the operator in the where clause
*@param value : where the field name has the value
*@return : object of this (query_builder)
*/
query_builder & query_builder::where(string field, string op, string value) {
	return  where(field, op, value, true, false, "AND");
}

/**
*Add a where clause joined by OR
*@param field : field_name
*@param op : the operator in the where clause
*@param value : where the field name has the value
*@return : object of this query_builder
*/
query_builder & query_builder::or_where(string field, string op, string value) {
	return  where(field, op, value, true, false, "OR");
}

/**
*Add a where clause using map
*@param data : a map with fields to form the where clause
*@return : object of this (query_builder)
*/
query_builder & query_builder::where(map<string, string> data) {
	
	map<string, string>::iterator it = data.begin();

	while(it != data.end()) {
		where(it->first, "=", it->second);
		it++;
	}
	
	return *this;
}

/**
*Add a or_where clause using map
*@param data : a map with fields to form the where clause
*@return : object of this (query_builder)
*/
query_builder & query_builder::or_where(map<string, string> data) {
	
	map<string, string>::iterator it = data.begin();

	while(it != data.end()) {
		or_where(it->first, "=", it->second);
	}
	return *this;
}

/**
*Add a where clause where the field is in the vector of the values
*@param field: field_name
*@param values : vector of values
*@return : an object of this query_builder
*/
query_builder & query_builder::where_in(string field, vector<string> values) {

	string clause = field + " in";
	string joiner = "AND";
	string fields_string;

	for(vector<int>::size_type i = 0; i < values.size(); i++) {
		if(i > 0) {
			fields_string += ", ?";
		} else {
			fields_string = "? ";
		}

		where_clause_values.push_back(values[i]);
	}

	if(fields_string.length() > 0) {
		clause += " ("+ fields_string + ")";
		pair<string, string> where_pair(joiner, clause);
		where_clause_chunks.push_back(where_pair);
	}

	return *this;
}

/**
*Add a where clause joined after OR where the field is in the vector of the values
*@param field: field_name
*@param values : vector of values
*@return : an object of this query_builder
*/
query_builder & query_builder::or_where_in(string field, vector<string> values) {

	string clause = field + " in";
	string joiner = "OR";
	string fields_string;

	for(vector<string>::size_type i = 0; i < values.size(); i++) {
		if(i > 0) {
			fields_string += ", ?";
		} else {
			fields_string = "? ";
		}

		where_clause_values.push_back(values[i]);
	}

	if(fields_string.length() > 0) {
		clause += " ("+ fields_string + ")";
		pair<string, string> where_pair(joiner, clause);
		where_clause_chunks.push_back(where_pair);
	}

	return *this;
}

/**
*push back order by field to order_by_chunks
*@param field :  field to order by
*@return : object of this query_builder
*/
query_builder & query_builder::order_by(string field) {
	//Order by is ASC by default.
	pair <string, string> chunk(field, "ASC");
	order_by_chunks.push_back(chunk);
	return *this;
}

/**
*push back order by field to order_by_chunks
*@param field :  field to order by
*@param order : the order to order by
*@return: object of this query_builder
*/
query_builder & query_builder::order_by(string field, string order) {
	pair <string, string> chunk(field, order);
	order_by_chunks.push_back(chunk);
	return *this;
}

/**
*Set results limit
*@param s : limit (a number)
*@return this: object of this query_builder
*/
query_builder & query_builder::limit(string s) {
	results_limit = s;
	return *this;
}


query_builder & query_builder::group_by(string field) {
	group_by_fields.push_back(field);
	return *this;
}

/**
*Make pagination
*@param items_per_page: number of items to be displayed per page
*@return this: object of this query_builder
*/
query_builder & query_builder::paginate(int items_per_page) {
	_pagination.set_items_per_page(items_per_page);
	_pagination.set_current_page(1);
	return *this;
}

/**
*Make pagination
*@param items_per_page: number of items to be displayed per page
*@param page number : Set to the current page (Which determines the offset)
*@return this: object of this query_builder
*/
query_builder & query_builder::paginate(int items_per_page,  int page_number) {
	_pagination.set_items_per_page(items_per_page);
	_pagination.set_current_page(page_number);
	return *this;
}

/**
*Make pagination
*@param items_per_page: number of items to be displayed per page
*@param page number : Set to the current page (Which determines the offset)
*@param total_items: Total number of items to be paginated
*@return this: object of this query_builder
*/
query_builder & query_builder::paginate(int items_per_page,  int page_number, int total_items) {

	_pagination.set_total_items(total_items);
	_pagination.set_items_per_page(items_per_page);
	_pagination.set_current_page(page_number);
	_pagination.paginate();

	results_limit = to_string(items_per_page);
	results_offset = to_string(_pagination.get_offset());
	return *this;
}

/**
*Join all the joins into one
*@return string: where_clauses
*/
string query_builder::build_joins() {
	return vector_to_string(joins, " ");
}

/**
*Join all the on clause chunks into one
*@return string: on_clauses
*/
string query_builder::build_on_clauses() {

	string on_clauses("");

	for(vector<int>::size_type i = 0;  i <  on_clause_chunks.size(); i++) {

		pair<string, string> chunk =  on_clause_chunks[i];

		if(i == 0) {
			on_clauses += " ON " + chunk.second + " ";
		} else {
			on_clauses +=  " " + chunk.first + " ";
			on_clauses +=  " " + chunk.second + " ";
		}
	}
	return on_clauses;
}

/**
*Join all the where clause chunks into one
*@return string: where_clauses
*/
string query_builder::build_where_clauses() {

	string where_clauses("");

	vector<string>::size_type total_where_clause_chunks = where_clause_chunks.size();

	for(vector<string>::size_type i = 0;  i <  total_where_clause_chunks; i++) {

		pair<string, string> chunk = where_clause_chunks[i];

		if(i == 0) {
			where_clauses += " WHERE " + chunk.second + " ";
		} else {
			where_clauses +=  " " + chunk.first + " ";
			where_clauses +=  " " + chunk.second + " ";
		}
	}


	return where_clauses;
}

string query_builder::build_group_by() {

	string group_by_string("");

	for(size_t i = 0; i < group_by_fields.size(); i++) {

		string field = group_by_fields[i];

		if(i == 0) {
			group_by_string += " GROUP BY " + field + " ";
		} else {
			group_by_string += ", " + field + " ";
		}
	}

	return  group_by_string;

}

/**
*Build the order by part of the select query
*@return string: the order by part
*/
string query_builder::build_order_by() {

	string s("");
	for(vector<string>::size_type i = 0; i < order_by_chunks.size(); i++) {

		if(s.length() > 0) {
			s += ", " + order_by_chunks[i].first;
			s += " " +  order_by_chunks[i].second;
		} else {
			s +=       order_by_chunks[i].first;
			s += " " + order_by_chunks[i].second;
		}
	}

	if(s.length() > 0) {
		s = "ORDER BY " + s;
	 }

	return s;
}

/**
*Build the limit part
*@return string: the limit part
*/
string query_builder::build_limit() {
	string s("");

	if(results_limit.length() > 0) {
		s = " LIMIT " + results_limit;
	}
	return s;
}

string query_builder::build_offset() {

	string s("");
	if(results_offset.length() > 0) {
		s = " OFFSET " + results_offset + " ";
	}

	return  s;
}



/**
*Build insert query_string
*/
void query_builder::build_insert_query() {

	for(size_t i = 0; i < insert_data.size(); i++) {

		string fields("");
		string values("");

		map<string, string> data = insert_data[i];
		map<string,string>::iterator it = data.begin();

		while(it != data.end() ) {

			if(fields.length() > 0 ) {
				fields += ", " + it->first; 
				values += ", ?";
			} else {
				fields +=  it->first;
				values +=  "?";
			}
			insert_values.push_back(it->second);
			it++;
		}

		if(i < 1) {	

		 _query << "INSERT INTO " + table_name + "(" + fields + ") "
    	  	 	<< "VALUES(" + values + ")";	
		} else {

			 _query << ", (" + values + ")";
		}
	}


}

/**
*Build select query_string
*/
void query_builder::build_select_query() {

	string fields_string = "*";

	if(select_fields.size() > 0 ) {
		fields_string = vector_to_string(select_fields, ", ");
	}

	_query << "SELECT "
		   << fields_string 
		   << " FROM " << table_name
		   << " " << build_joins() 
		   << build_on_clauses()
		   << build_where_clauses()
		   << build_group_by()
		   << build_order_by()
		   << build_limit()
		   << build_offset();
}

/**
*Build update query_string
*/
void query_builder::build_update_query() {

	map<string, string>::iterator it = update_data.begin();

	string middle_part("");

	while(it != update_data.end()) {
		if(middle_part.length() > 0) {
			middle_part += ", " + it->first + "=?";
		} else {
			middle_part += it->first + "=?";
		}

		update_values.push_back(it->second);
		it++;
	}

	_query << "UPDATE " + table_name + " SET "
	       << middle_part << build_where_clauses();
}

/**
*Build delete query_string
*/
void query_builder::build_remove_query() {
	_query << "DELETE FROM " + table_name + " "
		   << build_where_clauses();
}


/**
*Build query string
*@return: object of this query_builder
*/
query_builder & query_builder::build_query() {

	switch(query_type){
		case  _select:
			build_select_query();
			extend_vector(all_values, where_clause_values);
			extend_vector(all_values, on_clause_values);
		break;
		case _insert:
			build_insert_query();
			extend_vector(all_values, insert_values);
		break;
		case _update:
			build_update_query();
			extend_vector(all_values, update_values);
			extend_vector(all_values, where_clause_values);
		break;
		case _remove:
			build_remove_query();
			extend_vector(all_values, where_clause_values);
		break;
		case _raw:

		break;

		default:
			//cout << "Not the right type" << endl;
		break;
	}

	return *this;
}

/**
*Get all values (params of the prepared statement)
*@return the value of private property (all_values)
*/
vector<string> query_builder::get_all_values() {
	return all_values;
}

void query_builder::set_query(query q) {
	_query  = q;
}

/**
*Return the value of the private property (query_string)
*@return: query_string
*/
string query_builder::get_query_string() {
	return _query.str();
}

void query_builder::reset() {

	map<string, string> x;
	vector<string> y;
	vector< pair<string, string> > z;
	vector< map<string, string> > xy;



	insert_data = xy;
	insert_values = y;

	select_fields = y;
	where_clause_chunks = z;
	on_clause_chunks = z;
	order_by_chunks = z;
	joins = y;

	where_clause_values = y;
	on_clause_values = y;

	update_data = x;
	update_values = y;

	all_values = y;

	results_limit = "";
	results_offset = "";

	_query.reset();
}





}//efficient
}// database