/*
*pagination.cpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/

#include "pagination.hpp"

namespace efficient {
namespace database {


pagination::pagination(){
 	items_per_page = 0;
	current_page = 0;
	offset = 0;
}

/**
*pagination
*@param _items_per_page : (limit) number of items per page
*/
pagination::pagination(int _items_per_page) {
	items_per_page = _items_per_page;
	current_page = 0;
	offset = 0;
}


/**
*pagination
*@param _items_per_page : (limit) number of items per page
*@param _current_page :  already set to the given page number
*/
pagination::pagination(int _items_per_page, int _current_page) {
	items_per_page = _items_per_page;
	current_page   = _current_page;
}


/**
*Set total pages (Total pages in this pagination)
*Set value to the private property (total pages)
*@param _total_pages : new value to be set to total pages.
*/
void pagination::set_total_pages(int _total_pages) {
	total_pages = _total_pages;
}

/**
*Set total items: As in the total number of items eg rows etc
*Set value to the private property (total_items)
*@param _total_items: new value to be set to total_items
*/
void pagination::set_total_items(int _total_items) {
	total_items = _total_items;
}

/**
*Set items per page: A limit of items to be displayed per page
*Set value to the private property: _items_per_page
*@param _items_per_page: new value to be set to items_per_page
*/
void pagination::set_items_per_page(int _items_per_page) {
	items_per_page = _items_per_page;
}

/**
*Set the offset
*Set value to the private property (offset)
*@param _offset : new value to be set to offset
*/
void pagination::set_offset(int _offset) {
	offset = _offset;
}

/**
*Set page number as in the current page
*Set value to the private property (current_page)
*@param _current_page: The value to be set to current_page
*/
void pagination::set_current_page(int _current_page) {
	current_page =_current_page;
}

/**
*Get current page
*Get the value of the private property current_page
*@return : current page 
*/
int pagination::get_current_page() {
	return current_page;
}

/**
*Get total pages
*Get value of the private property (total_pages)
*@return : total_pages
*/
int pagination::get_total_pages() {
	return total_pages;
}

/**
*Get offset
*Get value of the private property (offset)
*@return : offset
*/
int pagination::get_offset() {
	return offset;
}

/**
*Make pagination
*/
void pagination::paginate() {
	offset = (current_page - 1) * items_per_page;
	double temp_value = (float) total_items / (float) items_per_page;
	total_pages = ceil(temp_value);
}






}// namespace database
}// namespace efficient