/*query.hpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/


#ifndef EFFICIENT_DATABASE_QUERY_HPP
#define EFFICIENT_DATABASE_QUERY_HPP

#include <iostream>
#include <string>
#include <sstream>

using namespace std;

namespace efficient {
namespace database{

class query{

public:
	query();
	query(string s);

	//Append string to query on istream
	query & operator << (string s);

	//get string value of query
	string str();

	//Determine if it is a select query.
	bool is_select();

	//Reset query string.
	void reset();
 
private:
	string query_string;


};


}//namespace database
}//namespace efficient




#endif //EFFICIENT_DATABASE_QUERY_HPP