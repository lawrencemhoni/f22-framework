/*
*pagination.hpp
*copyright 2016-2020 Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/
#ifndef EFFICIENT_DATABASE_PAGINATION_HPP
#define EFFICIENT_DATABASE_PAGINATION_HPP

#include <iostream>
#include <cmath>

using namespace std;

namespace efficient {
namespace database {

class pagination {


public:

	pagination();
	pagination(int _items_per_page);
	pagination(int _items_per_page, int _current_page);

	int get_total_pages();
	int get_total_items();
	int get_items_per_page();
	int get_offset();
	int get_current_page();

	void set_total_pages(int _total_pages);
	void set_total_items(int _total_items);
	void set_items_per_page(int _items_per_page);
	void set_offset(int _offset);
	void set_current_page(int _current_page);

	void paginate();

private:

	int total_pages;
	int total_items;
	int items_per_page;
	int offset;
	int current_page;

}; // pagination


}// namespace database
}// namespace effficient








#endif //EFFICIENT_DATABASE_PAGINATION_HPP