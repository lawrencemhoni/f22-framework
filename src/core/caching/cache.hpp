/*
*cache.cpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#ifndef EFFICIENT_CACHE_HPP
#define EFFICIENT_CACHE_HPP

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include "drivers/driver.hpp"

using namespace std;

using efficient::caching::drivers::driver;

namespace efficient {
namespace caching {


class cache{

public:

	//Get content of the cache key
	static string get(string key);

	//Check if cache has this key
	static bool has(string key);

	//Save to cache
	static void put(string key,  string value);

	//Save to cache with an expiry time in seconds
	static void put(string key, string value, int seconds);

	//Save to cache with an expiry time in seconds (string)
	static void put(string key, string value, string seconds);

	//Remove where this key
	static void remove(string key);

	//Set cache driver
	static void set_driver(driver * d);

	//Set cache key name prefix
	static void set_key_prefix(string prefix);

	//Get cache key name prefix
	static string get_key_prefix();

protected:
	static driver * _driver;
	static string key_prefix;

};


}//namespace caching
} //namespace efficient


#endif //EFFICIENT_CACHE_HPP