/*
*driver.hpp
*copyright (c) 2016-2020
*lawrencetmhoni@gmail.com
*/
#ifndef EFFICIENT_CACHING_DRIVERS_DRIVER_HPP
#define EFFICIENT_CACHING_DRIVERS_DRIVER_HPP

#include <iostream>

using namespace std;


namespace efficient {
namespace caching{
namespace drivers{


class driver {

public:
	driver();

	//return the value of key
	virtual string get(string key);

	//save to cache
	virtual void put(string key, string value);

	//save to cache with expiry time in seconds
	virtual void put(string key, string value, int seconds);

	//save to cache with expiry time in seconds (in string format)
	virtual void put(string key, string value, string seconds);

	//Remove where this key
	virtual void remove(string key);

	//Remove from cache where this key
	virtual bool has(string key);

	void set_key_prefix(string prefix);

protected:
	 string key_prefix;

};



}
}//namespace caching
}//namespace caching





#endif //EFFICIENT_CACHING_DRIVERS_DRIVER_HPP