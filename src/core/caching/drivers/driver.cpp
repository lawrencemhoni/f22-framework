/*
*driver.cpp
*copyright (c) 2016-2020 Lawrence T Mhoni 
*lawrencetmhoni@gmail.com
*/

#include "driver.hpp"

namespace efficient {
namespace caching {
namespace drivers{


driver::driver() {}

//return the value of key
string driver::get(string key) { return string(""); }
//save to cache
void driver::put(string key, string value) {}
//save to cache with expiry time in seconds
void driver::put(string key, string value, int expire) {}
//save to cache with expiry time in seconds  (in string format)
void driver::put(string key, string value, string expire) {}
//Check if cache has the key
bool driver::has(string key) { return  false; }
//Remove from cache where this key
void driver::remove(string key) { }

//set key prefix
void driver::set_key_prefix(string prefix) {
	key_prefix = prefix;
}



}//namespace drivers
}//namespace caching
}//namespace efficient