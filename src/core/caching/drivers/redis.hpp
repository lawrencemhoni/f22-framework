/*
*redis.hpp
*copyright (c) Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/


#ifndef EFFICIENT_CACHING_DRIVERS_REDIS_HPP
#define EFFICIENT_CACHING_DRIVERS_REDIS_HPP

#include <string>
#include "driver.hpp"
#include "redis/hiredis.h"

using namespace std;

namespace efficient{
namespace caching{
namespace drivers{

class redis : public driver{

public:

	redis();

	//Initiate a GET command
	virtual string get(string key);

	//Initiate a SET command
	virtual void put(string key, string value);

	//Initiate a SET command and Expire
	virtual void put(string key, string value, int seconds);

	//Initiate a SET command and Expire (seconds in string value)
	virtual void put(string key, string value, string seconds);

	//Initiate a DEL command
	virtual void remove(string key);

	//Initiate an EXISTS command
	virtual bool has(string key);


private:
	redisContext *connection;
    redisReply *reply; 

    //Create the connection to the redis server
    void connect(string host, int port);

    //Check if there is a connection to the redis server
    bool is_connected();

    //Initiate an EXPIRE command
    void _expire(string key, string expire);

    //Prepend a prefix to the key
    string determine_key_name(string & key);


};


}
}
}

#endif //EFFICIENT_CACHING_DRIVERS_REDIS_HPP