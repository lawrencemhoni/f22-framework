/*
*redis.cpp
*copyright (c) Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#include "redis.hpp"

namespace efficient{
namespace caching{
namespace drivers{

redis::redis(){
	
	connect("127.0.0.1", 6379);
}


/*
*Create the connection to the redis server
*@param host : Hostname
*@param port : port
*/
void  redis::connect(string host, int port) {
    struct timeval timeout = { 1, 500000 }; // 1.5 seconds
    connection = redisConnectWithTimeout(host.c_str(), port, timeout);
    if (connection == NULL || connection->err) {
        if (connection) {
            cout << "Connection error " <<  connection->errstr << endl;
            redisFree(connection);
        } else {
           cout << "Connection error: can't allocate redis context" << endl;
        }
    }
}

/*
*Check if there is a connection to the redis server
*@return bool
*/
bool redis::is_connected() {
	if (connection == NULL || connection->err) { 
		return  false;
	}
	return true;
}

/*
*Prepend a prefix to the key
*@return key with prefix
*/
string redis::determine_key_name(string & key) {
	key = key_prefix + key;
	return key;
}

/*
*Initiate a GET command
*@param key : key
*@return result
*/
string redis::get(string key) {
	string result = string("");
	determine_key_name(key);
	if(is_connected() && has(key)){
    	reply = redisCommand(connection,"GET %s", key.c_str());
    	result = string(reply->str);
	}    
 
    freeReplyObject(reply);
	return result;
}

/*
*Initiate a SET command
*@param key : key
*@param value : value to be inserted
*/
void redis::put(string key, string value) {
	if(is_connected()) {
		determine_key_name(key);
		redisCommand(connection,"SET %s %s", key.c_str(), value.c_str());		
	}
}

/*
*Initiate a SET command
*@param key : key
*@param value : value to be inserted
*@param expire : number of seconds to expire.
*/
void redis::put(string key, string value, int expire) {
	 put(key, value, to_string(expire));
}

/*
*Initiate a SET command
*@param key : key
*@param value : value to be inserted
*@param expire : number of seconds to expire in string format.
*/
void redis::put(string key, string value, string expire) {
	 put(key, value);
	 _expire(key, expire);
}

/*
*Initiate a DEL command
*@param key : key
*/
void redis::remove(string key) {
	if(is_connected()) {
		determine_key_name(key);
		redisCommand(connection,"DEL %s", key.c_str());
	}
}

/*
*Initiate an EXPIRE command
*@param key : key to be expired
*@param expire : number of seconds in string that key should expire
*/
void redis::_expire(string key, string expire) {
	if(is_connected()) {
		 determine_key_name(key);
		 redisCommand(connection,"EXPIRE %s %s", 
		 key.c_str(), expire.c_str());
	}
}

/*
*Check if key exists
*@param key : key 
*@bool
*/
bool redis::has(string key ) {
	if(is_connected()){
		 reply = redisCommand(connection,"EXISTS %s", key.c_str());
		 if(reply->integer == 1) {
		 	return true;
		 }
	}
	return false;
}



} //namespace drivers
}//namespace caching
}//namespace efficient