/*
*cache.cpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/
#include "cache.hpp"

namespace efficient {
namespace caching{


driver * cache::_driver;
string cache::key_prefix;

/*
*Set cache driver
*@param d: cache driver
*/
void cache::set_driver(driver * d) {
	_driver = d;
	_driver->set_key_prefix(key_prefix);
}


/*
*Set cache key name prefix
*@param prefix: prefix
*/
void cache::set_key_prefix(string prefix){
	key_prefix = prefix;
}


/*
*Get cache key name prefix
*@return: key_prefix
*/
string cache::get_key_prefix(){
	return key_prefix;
}

/*
*Get content of the cache key
*@param key : cache key
*@return : content
*/
string cache::get(string key) {
	return _driver->get(key);
}

/*
*Save to cache 
*@param key :  key
*@param value : content to be saved
*/
void cache::put(string key, string value) {
	_driver->put(key, value);
}


/*
*Save to cache with an expiry time in seconds
*@param key :  key
*@param value : content to be saved
*@expiry: value of seconds of content expiry
*/
void cache::put(string key, string value, int expiry) {
	_driver->put(key, value, expiry);
}

/*
*Save to cache with an expiry time in seconds
*@param key :  key
*@param value : content to be saved
*@expiry: string value of seconds of content expiry
*/
void cache::put(string key, string value, string expiry) {
	_driver->put(key, value, expiry);
}

/*
*Remove where this key
*@param key: key
*/
void cache::remove(string key) {
	_driver->remove(key);
}

/*
*Check if cache has this key
*@param key: key
*@return bool
*/
bool cache::has(string key) {
	return _driver->has(key);
}






}//caching
}//efficient