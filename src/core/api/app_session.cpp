/*app_session.cpp
*copyright (c) 2016 - 2020 Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/


#include "app_session.hpp"

namespace efficient {
namespace api{

app_session::app_session(){

	set_table_name("app_sessions");
	set_primary_key("id");

}

}//namespace models
}//app

