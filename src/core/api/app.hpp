/*app.hpp
*copyright (c) 2016 - 2020 Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/

#ifndef EFFICIENT_API_APP_HPP
#define EFFICIENT_API_APP_HPP

#include "../database/model.hpp"

using namespace efficient::database;

namespace efficient {
namespace api{

class app  : public  model {

public :
	app();

};

}//namespace models
}//app

#endif