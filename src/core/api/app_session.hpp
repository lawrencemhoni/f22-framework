/*app_session.hpp
*copyright (c) 2016 - 2020 Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/

#ifndef EFFICIENT_API_APP_SESSION_HPP
#define EFFICIENT_API_APP_SESSION_HPP

#include "../database/model.hpp"

using namespace efficient::database;

namespace efficient {
namespace api{

class app_session  : public  model {

public :
	app_session();

};

}//namespace models
}//app

#endif