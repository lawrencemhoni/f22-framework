/*
*auth.cpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#include "auth.hpp"

namespace efficient {
namespace api {

auth::auth(request req) {
	_request = req;
}


bool auth::check() {

	if(_request.get_get_input("app_id").length() > 0) {
		string id = _request.get_input["app_id"];
		string user_agent = _request.get_header("user-agent");
		string api_key = _request.get_get_input("api_key");
		string cache_key   = "auth-app-" + id;
		string cache_value = cache::get(cache_key);

		string session_id;

		string user_agent_pattern = "agent-" + user_agent + "-sess-(.*)";

		if(boost::regex_match(cache_value, boost::regex(user_agent_pattern)) ){

		 	string id_pattern  = "(.*)-sess-";
		 	session_id = boost::regex_replace(cache_value, boost::regex(id_pattern), "");

		 	cout << "App is authenticated. id: "<< session_id << endl;

		 	return true;
		}
	}

	cout << "App is not yet authenticated." << endl;
	return false;
}


bool auth::attempt() {

	if(_request.get_get_input("app_id").length() > 0) {

		if(check()) {
			return true;
		}

		string id         = _request.get_input["app_id"];
		string api_key    = _request.get_get_input("api_key");
		string user_agent = _request.get_header("user-agent");
		string cache_key = "auth-app-" + id;

		string cache_value;

		try {
			efficient::api::app _app;

				_app.select(_argsv("id", "api_key"))
				    .where("id", "=", id)
				    .where("user_agent", "=", user_agent)
				    .where("api_key", "=", api_key)
				    .where("status", "=", "1")
				    .first_or_fail();

			map<string, string> session_data;

			session_data["app_id"] = id;

			 app_session _app_session;

			_app_session.first_after_create(session_data);

			cache_value =  "agent-" + user_agent + "-sess-" + _app_session["id"];

			cache::put(cache_key, cache_value, 60 * 15);
			return true;

		}catch(model_not_found_exception e) {
			//Here we will log the failure.
		}
	}

	return false;
}

void auth::force_session_destroy(string id) {
	string cache_key = "auth-app-" + id;
	cache::remove(cache_key);
}


}//namespace api
}//namespace efficient