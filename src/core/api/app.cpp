/*app.cpp
*copyright (c) 2016 - 2020 Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/


#include "app.hpp"

namespace efficient {
namespace api{

app::app(){

	set_table_name("apps");
	set_primary_key("id");

}

}//namespace models
}//app

