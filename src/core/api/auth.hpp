/*auth.hpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/


#ifndef EFFICIENT_API_AUTH_HPP
#define EFFICIENT_API_AUTH_HPP

#include <iostream>

#include <boost/regex.hpp>

#include "../http/request.hpp"
#include "../generic/helpers.hpp"
#include "../generic/calendar.hpp"
#include "../caching/cache.hpp"
#include "../caching/drivers/redis.hpp"
#include "../database/exceptions/model_not_found_exception.hpp"
#include "app_session.hpp"
#include "app.hpp"


using namespace generic;
using namespace generic::helpers;
using namespace efficient::http;
using namespace efficient::database::exceptions;

using efficient::caching::cache;
using efficient::caching::drivers::redis;

namespace efficient {
namespace api {

class auth{

public:

	auth(request req);

	bool check();

	bool attempt();

	void static force_session_destroy(string id);

protected:
	request _request;

};




}//namespace api
}//namespace efficient

#endif //EFFICIENT_API_AUTH_HPP