/*
*sweetheart.hpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#ifndef EFFICIENT_SECURITY_SWEETHEART_HPP
#define EFFICIENT_SECURITY_SWEETHEART_HPP


#include <iostream>
#include <string>
#include <cstdlib>
#include <assert.h>
#include <crypto++/cryptlib.h>
#include <crypto++/osrng.h>
#include <crypto++/hex.h>
#include <crypto++/filters.h>
#include <crypto++/aes.h>
#include <crypto++/blowfish.h>
#include <crypto++/ccm.h>
#include <crypto++/eax.h>
#include <cryptopp/sha.h>
#include <crypto++/pwdbased.h>
#include <cryptopp/filters.h>
#include <cryptopp/hex.h>
#include <crypto++/secblock.h>

using namespace std;
using namespace CryptoPP;


namespace efficient {
namespace security {


class sweetheart{


public:

sweetheart();

enum {
	_aes,
	_blowfish
} enc_type;

//Derive key from string
void derive_key(string new_key);

//Number of iterations
void set_iterations(int new_iterations);

//Encrypt with the selected encryptor
void encrypt(string content);

//Decrypt with the selected decryptor
void decode_decrypt(string);

void encrypt_with_blowfish(string content);

void encrypt_with_aes(string content);

void decrypt_with_blowfish(string encrypted);

void decrypt_with_aes(string encrypted);

string get_cipher_text();

string get_encoded_cipher_text();

string get_recovered_text();

void encode(string &decoded_source, string &encoded_text );

void decode(string &encoded_source, string &decoded_text );

private:

SecByteBlock derived_key;
unsigned int iterations;

string cipher_text;
string recovered_text;



};




}//namespace security
}//namespace efficient



#endif //EFFICIENT_SECURITY_CIPHER_HPP