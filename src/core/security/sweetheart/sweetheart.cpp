/*
*sweetheart.hpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/


#include "sweetheart.hpp"

namespace efficient {
namespace security {

sweetheart::sweetheart() {
	iterations =  15000;
	derive_key("super secrete password");

}

/**
*Derive key from string
*@param new_key: The value of the new key
*/
void sweetheart::derive_key(string new_key) {

	    char purpose = 0; 

	    SecByteBlock newkey(32);

	    derived_key = newkey;

	    PKCS5_PBKDF2_HMAC<SHA256> kdf;
	    kdf.DeriveKey(derived_key.data(), derived_key.size(), purpose, (byte*) new_key.data(), new_key.size(), NULL, 0, iterations);

}

/**
*Encrypt with the selected encryptor
*@param content: content to be encrypted
*/
void sweetheart::encrypt(string content) {

	switch(enc_type) {
		case _blowfish:
		encrypt_with_blowfish(content);
		break;

		case _aes:
		encrypt_with_aes(content);
		break;

		default:
		encrypt_with_aes(content);
		break;
	}

}


/**
*Encrypt with the blowfish encryptor
*@param content: content to be encrypted
*/
void sweetheart::encrypt_with_blowfish(string content) {  	
  	EAX<Blowfish>::Encryption encryptor;

  	encryptor.SetKeyWithIV(derived_key.data(), 16, derived_key.data() + 16, 16);

    AuthenticatedEncryptionFilter ef(encryptor, new StringSink(cipher_text));
    ef.Put((byte*)content.data(), content.size());
    ef.MessageEnd();
}


/**
*Encrypt with the AES encryptor
*@param content: content to be encrypted
*/
void sweetheart::encrypt_with_aes(string content) {
  	
  	EAX<AES>::Encryption encryptor;

  	encryptor.SetKeyWithIV(derived_key.data(), 16, derived_key.data() + 16, 16);

    AuthenticatedEncryptionFilter ef(encryptor, new StringSink(cipher_text));
    ef.Put((byte*)content.data(), content.size());
    ef.MessageEnd();
}


/**
*Decode the hex content and decrypt
*@param encoded_encrypted: content to be decded and decrypted
*/
void sweetheart::decode_decrypt(string encoded_encrypted) {

	string decoded_encrypted;

	decode(encoded_encrypted, decoded_encrypted);

	switch(enc_type) {
		case _blowfish:
		decrypt_with_blowfish(decoded_encrypted);
		break;

		case _aes:
		decrypt_with_aes(decoded_encrypted);
		break;

		default:
		decrypt_with_aes(decoded_encrypted);
		break;
	}
}



/**
*Decrypt with the AES decrytor
*@param content: content to be decrypted
*/
void sweetheart::decrypt_with_aes(string encrypted) {

    EAX<AES>::Decryption decryptor;
    decryptor.SetKeyWithIV(derived_key.data(), 16, derived_key.data() + 16, 16);

    AuthenticatedDecryptionFilter df(decryptor, new StringSink(recovered_text));
    df.Put((byte*)encrypted.data(), encrypted.size());
    df.MessageEnd();
}

/**
*Decrypt with the blowfish decrytor
*@param content: content to be decrypted
*/
void sweetheart::decrypt_with_blowfish(string encrypted) {

    EAX<Blowfish>::Decryption decryptor;
    decryptor.SetKeyWithIV(derived_key.data(), 16, derived_key.data() + 16, 16);

    AuthenticatedDecryptionFilter df(decryptor, new StringSink(recovered_text));
    df.Put((byte*)encrypted.data(), encrypted.size());
    df.MessageEnd();
}

/**
*Encode content from bytes to hex
*@param decoded_source: The content that is in bytes
*@param encoded_text:  Where content is stored when encoded
*/
void sweetheart::encode(string &decoded_source, string &encoded_text ) {
	HexEncoder encoder;

    encoder.Detach(new StringSink(encoded_text));
    encoder.Put((byte*)decoded_source.data(), decoded_source.size());
    encoder.MessageEnd();
}


/**
*Encode content from hex to bytes
*@param encoded_source: The content that is encoded in hex
*@param decoded_text:  Where content is stored when decoded
*/
void sweetheart::decode(string &encoded_source, string &decoded_text ) {

		HexDecoder decoder;
		 
		decoder.Attach( new StringSink( decoded_text ) );
		decoder.Put( (byte*)encoded_source.data(), encoded_source.size() );
		decoder.MessageEnd();
}


/**
*Get cipher text that is encoded to hex
*@return : encoded text
*/
string sweetheart::get_encoded_cipher_text() {
    string encoded_cipher_text;
    encode(cipher_text, encoded_cipher_text);
	return encoded_cipher_text;
}

/**
*Get raw cipher text that is in bytes
*@return : cipher_text
*/
string sweetheart::get_cipher_text() {
	return cipher_text;
}

/**
*Get recovered text when decrypted
*@return: recovered_text.
*/
string sweetheart::get_recovered_text() {
	return recovered_text;
}



}//namespace security
}//namespace efficient