/*hash.hpp
*copyright 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#ifndef EFFICIENT_SECURITY_HASH_HPP
#define EFFICIENT_SECURITY_HASH_HPP

#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;


namespace efficient {
namespace security {


class hash {

public:
	static string make(string password);
	static bool verify_password(string password, string hash);
	static void set_salt_key(string _salt_key);
	static void set_random_salt_key(); 

private:
	static string salt_key;

};







} //security
} //namespace efficient



#endif //EFFICIENT_SECURITY_HASH_HPP