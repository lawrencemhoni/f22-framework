/*hash.c
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#include <iostream>
#include "hash.hpp"
#include "../bcrypt/final.h"

using namespace std;

namespace efficient {
namespace security {

string hash::salt_key;

string hash::make(string password) {

	string hash;

	int size = 0x12345678;

	char setting[CRYPT_GENSALT_OUTPUT_SIZE];
	char output[CRYPT_OUTPUT_SIZE];

	if(strlen(salt_key.c_str()) < 13 ) {
		set_random_salt_key();
		cout << "Setting random key" << endl;
	}

 	strcpy(setting, __crypt_gensalt("$2y$", 0, salt_key.c_str(), size));

	return  string( crypt_rn(password.c_str(), setting, output, sizeof(output)) );

}

void hash::set_salt_key(string _salt_key) {
	salt_key = _salt_key;
}

bool hash::verify_password(string password, string hash) {
	return _verify_password(password.c_str(), hash.c_str());
}

void hash::set_random_salt_key() {
		unsigned int len = 32;
		char s[len];

		srand (time(NULL));

	    const char alphanum[] =
	        "0123456789"
	        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	        "abcdefghijklmnopqrstuvwxyz";

	    for (unsigned int i = 0; i < len; ++i) {
	        s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
	    }

	    s[len] = 0;

	    salt_key = string(s);
}



} //namespace security
}//namespace efficient