/*
*bootstrap.hpp
*copyright (c) Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#ifndef EFFICIENT_BOOTSTRAP_HPP
#define EFFICIENT_BOOTSTRAP_HPP


#include <iostream>
#include "../generic/log.hpp"
#include "../generic/config.hpp"
#include "../caching/cache.hpp"
#include "../caching/drivers/redis.hpp"
#include "../http/server/server.hpp"
#include "../http/route.hpp"



using namespace std;
using namespace generic;
using namespace efficient::caching;
using namespace efficient::caching::drivers;

using efficient::http::server3::server;
using efficient::http::request_handler;

namespace efficient {

class bootstrap{

public:
	
static void load();



};



}//namespace efficient

#endif //EFFICIENT_BOOTSTRAP_HPP
