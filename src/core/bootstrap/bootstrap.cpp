/*
*bootstrap.cpp
*copyright (c) Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/


#include "bootstrap.hpp"


namespace efficient {


void bootstrap::load() {

	//Set directrory name for log files
	log::set_logs_dirname("tests/logs");

	//Set directory name for config files
	config::set_config_dirname("tests/config");

	//Load config resources
	config::load_resources();

	//Set the prefix of cache key
	cache::set_key_prefix("sb-api-cache:");
	
	//Set a default cache driver
	cache::set_driver(new redis());

	string server_address = config::get_string("server.address");
	string server_port    = config::get_string("server.port");
	string document_root  = config::get_string("server.document_root");
	size_t server_threads = config::get_int("server.threads");

	server proxy(server_address, server_port, document_root, server_threads);
	proxy.run();


}



}//namespace efficient

