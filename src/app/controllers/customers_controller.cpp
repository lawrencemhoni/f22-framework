/*
*customers_controller.cpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/
#include "customers_controller.hpp"

using app::models::customer;
using app::models::customer_mobile;

namespace app {
namespace controllers {

customers_controller::customers_controller() {

}


void customers_controller::index(request req, response &res) {

	ptree root;
	ptree json_customers;
	
	customer _customer;

	_customer.select()
			 .with("mobiles")
			 .paginate(20);

	while(_customer.has_next()) {

		ptree json_customer;

		json_customer.put("id", _customer["id"]);
		json_customer.put("first_name", _customer["first_name"]);
		json_customer.put("last_name", _customer["last_name"]);
		json_customer.put("email", _customer["email"]);

		customer_mobile _mobile;

		_customer.children("mobiles", _mobile);

		ptree json_mobiles;

		while(_mobile.has_next()) {

			ptree json_mobile;
			string decrypted_msisdn;

			if(customer_service::decrypt_msisdn(
				_mobile["msisdn"], 
				decrypted_msisdn)) {

				json_mobile.put("", decrypted_msisdn );
				json_mobiles.push_back(std::make_pair("", json_mobile));
			};
		}

		json_customer.add_child("mobiles", json_mobiles);
		json_customers.push_back(std::make_pair("", json_customer));
	}

	root.add_child("results", json_customers);

	std::ostringstream buf; 
	write_json (buf, root, false);

	std::string json = buf.str(); 

	res.send(json, response::ok, "application/json");
}



void customers_controller::view(request req, response &res) {

	try{

		customer _customer;
		_customer.select()
		 .where("id", "=", req.uri_params["id"])
		 .with("mobiles")
		 .first_or_fail();

		ptree root;
		ptree json_customer;

		json_customer.put("id", _customer["id"]);
		json_customer.put("first_name", _customer["first_name"]);
		json_customer.put("last_name", _customer["last_name"]);
		json_customer.put("email", _customer["email"]);

		customer_mobile _mobile;
		_customer.children("mobiles", _mobile);

		ptree json_mobiles;

		while(_mobile.has_next()) {

			ptree json_mobile;
			string decrypted_msisdn;

			if(customer_service::decrypt_msisdn(
				_mobile["msisdn"], 
				decrypted_msisdn) ) {
				json_mobile.put("", decrypted_msisdn );
				json_mobiles.push_back(std::make_pair("", json_mobile));
			}
		}

		json_customer.add_child("mobiles", json_mobiles);
		root.add_child("results", json_customer);
		std::ostringstream buf; 
		write_json (buf, root, false);

		std::string json = buf.str(); 

		res.send(json, response::ok, "application/json");

	}catch(model_not_found_exception e) {

	}

}

void customers_controller::create(request req, response &res) {

	map<string, string> data;

	data["first_name"] = "John";
	data["last_name"] = "Doe";
	data["email"] = "jdoe@gmail.com";

	customer _customer;

	_customer.create_or_update(data, _argsv("email"));

	string msisdn = "265882139007";
	string encrypted_msisdn;

	customer_service::encrypt_msisdn(msisdn, encrypted_msisdn);

	map<string, string> mobile_data;

	mobile_data["customer_id"] = _customer["id"];
	mobile_data["msisdn"]      =  encrypted_msisdn;
 
	customer_mobile _mobile;
	_mobile.create_or_update(mobile_data, _argsv("customer_id"));
}



}//namespace controllers
}//namespace app