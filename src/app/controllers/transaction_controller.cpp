/*
*transaction_controller.cpp
*This is an F22 controller
*lawrencetmhoni@gmail.com
*/

#include "transaction_controller.hpp"


namespace app {
namespace controllers {

	
/**
*Index records
*@param req: request
*@param res: response
*/
void transaction_controller::index(request req, response &res) {

}

/**
*View a record
*@param req: request
*@param res: response
*/
void transaction_controller::view(request req, response &res) {

}


/**
*Store a record
*@param req: request
*@param res: response
*/
void transaction_controller::store(request req, response &res) {

	stringstream data_stream;

	data_stream << req.get_body();

	string app_id = req.get_get_input("app_id");

	ptree root;
	ptree json_transaction;

	map <string, string> data;

	vector< map<string, string> > products;

	try{

		read_json(data_stream, json_transaction);

	    data["trans_id"] = json_transaction.get<string>("trans_id");
	    data["organisation_id"] = json_transaction.get<string>("organisation_id");
	    data["discount"] = json_transaction.get<string>("discount");

	    data["trans_code"]  = transaction_service::generate_trans_code(
	       					  data["organisation_id"], app_id, data["trans_id"]);

	    data["pass_code"]  = transaction_service::generate_pass_code();
	    data["qr_code"]    = transaction_service::generate_qr_code(
	    					 data["trans_code"], data["pass_code"]);

	    transaction _transaction;

	    _transaction.select()
	    			.where("trans_id", "=", data["trans_id"])
	    			.where("organisation_id", "=", data["organisation_id"])
	    			.first_or_fail();

		string result = "{}"; 
		res.send(result, response::conflict, "application/json");

	} catch(model_not_found_exception e) {

		transaction _transaction;

		_transaction.first_after_create(data);

		json_transaction.put("id", _transaction["id"]);
		json_transaction.put("trans_code", _transaction["trans_code"]);
		json_transaction.put("pass_code", _transaction["pass_code"]);
		json_transaction.put("qr_code", _transaction["qr_code"]);
		json_transaction.put("updated_at",  _transaction["updated_at"]);
		json_transaction.put("created_at",  _transaction["created_at"]);

	    if(json_transaction.count("products") > 0) {

	    	transaction_product _product;

		    BOOST_FOREACH(boost::property_tree::ptree::value_type &v, json_transaction.get_child("products"))
		    {
		    	map<string, string> product_data;

		    	product_data["transaction_id"] = _transaction["id"];
		    	product_data["name"] = v.second.get<string>("name");
		    	product_data["item_number"] = v.second.get<string>("item_number");
		    	product_data["serial_number"] = v.second.get<string>("serial_number");
		    	product_data["description"] = v.second.get<string>("description");
		    	product_data["price"] = v.second.get<string>("price");
		    	product_data["quantity"] = v.second.get<string>("quantity");
		    	product_data["discount"] = v.second.get<string>("discount");

		    	_product.insert(product_data, false);
		    }

		   _product.build();
		   _product.execute_update();
	    }

		root.add_child("data", json_transaction);

    	std::ostringstream buf; 
		write_json (buf, root, false);
		std::string json = buf.str(); 
		res.send(json, response::created, "application/json");

    } catch(...) {


    }
}


/**
*Update a record
*@param req: request
*@param res: response
*/
void transaction_controller::update(request req, response &res) {

}


/**
*Delete a record
*@param req: request
*@param res: response
*/
void transaction_controller::destroy(request req, response &res) {

}



}//namespace controllers
}//namespace app