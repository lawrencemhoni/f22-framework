/*
*organisation_controller.cpp
*This is an F22 controller
*lawrencetmhoni@gmail.com
*/

#include "organisation_controller.hpp"


namespace app {
namespace controllers {

	
/**
*Index records
*@param req: request
*@param res: response
*/
void organisation_controller::index(request req, response &res) {

		app::models::organisation _org;

		int items_per_page = string_to_int(
			req.get_get_input("items_per_page"), 15);

		int page_number = string_to_int(
			req.get_get_input("page"), 1);

		_org.select();

		_org.paginate(items_per_page, page_number);

		ptree root;
		ptree json_orgs;

		while(_org.has_next()) {

			ptree json_org;

			json_org.put("id", _org["id"]);
			json_org.put("name", _org["name"]);
			json_org.put("email", _org["email"]);
			json_org.put("physical_address", _org["physical_address"]);
			json_org.put("logo", _org["logo"]);
			json_org.put("created_at", _org["created_at"]);
			json_org.put("updated_at", _org["updated_at"]);

			json_orgs.push_back(std::make_pair("", json_org));
		}

		root.put("page", page_number);
		root.put("page_records", _org.rows_count());
		root.put("total_records", _org.get_total_records());
		root.add_child("data", json_orgs);

		std::ostringstream buf; 
		write_json (buf, root, false);
		std::string json = buf.str(); 
		res.send(json, response::ok, "application/json");

}

/**
*View a record
*@param req: request
*@param res: response
*/
void organisation_controller::view(request req, response &res) {

	ptree json_org;
	ptree root;

	string id = req.get_uri_param("id");

	try{

		app::models::organisation _org;

		_org.select() 
			.where("id", "=", id)
			.first_or_fail();

		json_org.put("id", _org["id"]);
		json_org.put("name", _org["name"]);
		json_org.put("email", _org["email"]);
		json_org.put("physical_address", _org["physical_address"]);
		json_org.put("logo", _org["logo"]);
		json_org.put("updated_at",  _org["updated_at"]);
		json_org.put("created_at",  _org["created_at"]);

		root.add_child("data", json_org);

    	std::ostringstream buf; 
		write_json (buf, root, false);
		std::string json = buf.str(); 
		res.send(json, response::ok, "application/json");

	} catch(model_not_found_exception e) {

		string text = "{}";
		res.send(text, response::no_content, "application/json");    
	}
}


/**
*Store a record
*@param req: request
*@param res: response
*/
void organisation_controller::store(request req, response &res) {

	stringstream data_stream;
	data_stream << req.get_body();

	ptree root;
	ptree json_org;

	map<string, string> data;

	try{
		read_json(data_stream, json_org);

	    data["name"]  = json_org.get<string>("name");
	    data["email"] = json_org.get<string>("email");
	    data["logo"] = json_org.get<string>("logo");
	    data["physical_address"] = json_org.get<string>("physical_address");

	    app::models::organisation _org;
    	_org.select()
    	    .where("email", "=", data["email"])
    	    .first_or_fail();

    	std::ostringstream buf; 
		write_json (buf, root, false);
		std::string json = buf.str(); 
		res.send(json, response::conflict, "application/json");

	} catch(model_not_found_exception e) {

		app::models::organisation _org;
		_org.first_after_create(data);

		json_org.put("id", _org["id"]);

		root.add_child("data", json_org);
    	std::ostringstream buf; 
		write_json (buf, root, false);
		std::string json = buf.str(); 
		res.send(json, response::created, "application/json");

    } catch(...) {
    	std::ostringstream buf; 
		write_json (buf, root, false);
		std::string json = buf.str(); 
		res.send(json, response::bad_request, "application/json");    	
    }
}


/**
*Update a record
*@param req: request
*@param res: response
*/
void organisation_controller::update(request req, response &res) {


	stringstream data_stream;
	data_stream << req.get_body();

	ptree root;
	ptree json_org;

	string id = req.get_uri_param("id");

	map<string, string> data;

	try{
		read_json(data_stream, json_org);

	    data["name"]  = json_org.get<string>("name");
	    data["email"] = json_org.get<string>("email");
	    data["logo"] = json_org.get<string>("logo");
	    data["physical_address"] = json_org.get<string>("physical_address");

	    app::models::organisation _org;
    	_org.select()
    	    .where("id", "=", id)
    	    .first_or_fail();

    	json_org.put("id", _org["id"]);

    	_org.where("id", "=", id)
    		.update(data);

    	root.add_child("data", json_org);
    	std::ostringstream buf; 
		write_json (buf, root, false);
		std::string json = buf.str(); 
		res.send(json, response::ok, "application/json");

	} catch(model_not_found_exception e) {

		std::ostringstream buf; 
		write_json (buf, root, false);
		std::string json = buf.str(); 
		res.send(json, response::no_content, "application/json");


    } catch(...) {
    	std::ostringstream buf; 
		write_json (buf, root, false);
		std::string json = buf.str(); 
		res.send(json, response::bad_request, "application/json");    	
    }

}


/**
*Delete a record
*@param req: request
*@param res: response
*/
void organisation_controller::destroy(request req, response &res) {

}



}//namespace controllers
}//namespace app