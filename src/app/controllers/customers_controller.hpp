#ifndef APP_CONTROLLERS_CUSTOMERS_CONTROLLERS_HPP
#define APP_CONTROLLERS_CUSTOMERS_CONTROLLERS_HPP

#include <map>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <crypto++/cryptlib.h>

#include "base_controller.hpp"
#include "../models/customer.hpp"
#include "../models/customer_mobile.hpp"
#include "../../core/generic/log.hpp"
#include "../../core/generic/helpers.hpp"
#include "../services/customer_service.hpp"


using namespace std;
using namespace efficient::http;
using namespace app::services;

using generic::log;

using boost::property_tree::ptree;
using boost::property_tree::read_json;
using boost::property_tree::write_json;


namespace app{
namespace controllers{ 


	class customers_controller{

	public:

		static void index(request req, response &res);

		static void create(request req, response &res);

		static void view(request req, response &res);


	private:
		customers_controller();
	}; //class users controller


} //namespace controllers
} //namespace app


#endif //APP_CONTROLLERS_CUSTOMERS_CONTROLLERS_HPP
