/*
*organisation_controller.hpp
*This is an F22 controller
*lawrencetmhoni@gmail.com
*/

#ifndef APP_CONTROLLERS_ORGANISATION_CONTROLLER_HPP
#define APP_CONTROLLERS_ORGANISATION_CONTROLLER_HPP

#include <map>
#include <sstream>
#include <stdexcept>
#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>


#include "base_controller.hpp"
#include "../models/organisation.hpp"

using namespace std;
using namespace efficient::http;

using namespace std;
using namespace efficient::http;
using namespace app::models;


using boost::property_tree::ptree;
using boost::property_tree::read_json;
using boost::property_tree::write_json;

using efficient::database::exceptions::model_not_found_exception;


namespace app{
namespace controllers{ 


class organisation_controller {

public:

/**
*Index records
*@param req: request
*@param res: response
*/
static void index(request req, response &res);

/**
*View a record
*@param req: request
*@param res: response
*/
static void view(request req, response &res);

/**
*store a record
*@param req: request
*@param res: response
*/
static void store(request req, response &res);

/**
*Update a record
*@param req: request
*@param res: response
*/
static void update(request req, response &res);
	

/**
*Destroy a record
*@param req: request
*@param res: response
*/
static void destroy(request req, response &res);

}; //class organisation_controller


} //namespace controllers
} //namespace app


#endif //APP_CONTROLLERS_ORGANISATION_CONTROLLER_HPP
