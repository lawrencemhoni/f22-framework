/*
*app_controller.cpp
*This is an F22 controller
*lawrencetmhoni@gmail.com
*/

#include "app_controller.hpp"

namespace app {
namespace controllers {

	
/**
*Index records
*@param req: request
*@param res: response
*/
void app_controller::index(request req, response &res) {

		app::models::app _app;

		int items_per_page = string_to_int(
			req.get_get_input("items_per_page"), 15);

		int page_number = string_to_int(
			req.get_get_input("page"), 1);

		string type_id  = req.get_get_input("type_id");
		
		_app.select();

		if(type_id.length() > 0) {
			_app.where("type_id", "=", type_id);
		}

		_app.paginate(items_per_page, page_number);

		ptree root;
		ptree json_apps;

		while(_app.has_next()) {
			ptree json_app;
			json_app.put("id", _app["id"]);
			json_app.put("name", _app["name"]);
			json_app.put("user_agent", _app["user_agent"]);
			json_app.put("api_key", _app["api_key"]);
			json_app.put("type_id", _app["type_id"]);
			json_app.put("status",  _app["status"]);
			json_app.put("updated_at",  _app["updated_at"]);
			json_app.put("created_at",  _app["created_at"]);

			json_apps.push_back(std::make_pair("", json_app));
		}

		root.put("page", page_number);
		root.put("page_records", _app.rows_count());
		root.put("total_records", _app.get_total_records());
		root.add_child("data", json_apps);

		std::ostringstream buf; 
		write_json (buf, root, false);
		std::string json = buf.str(); 
		res.send(json, response::ok, "application/json");

}

/**
*View a record
*@param req: request
*@param res: response
*/
void app_controller::view(request req, response &res) {

	ptree json_app;
	ptree json_sessions;
	ptree json_permissions;
	ptree root;

	string id = req.get_uri_param("id");

	try{

		app::models::app _app;

		app::models::app_session _session;
		app::models::app_permission _permission;

		_app.find_detailed(id);

		json_app.put("id", _app["id"]);
		json_app.put("name", _app["name"]);
		json_app.put("user_agent", _app["user_agent"]);
		json_app.put("api_key", _app["api_key"]);
		json_app.put("type_id", _app["type_id"]);
		json_app.put("status",  _app["status"]);

		_app.children("recent_sessions", _session);
		_app.children("permissions", _permission);

		while(_permission.has_next()) {

			ptree json_permission;
			json_permission.put("", _permission["id"] );
			json_permissions.push_back(std::make_pair("", json_permission));
		}

		while(_session.has_next()) {

			ptree json_session;
			json_session.put("id", _session["id"] );
			json_session.put("created_at", _session["created_at"] );
			json_session.put("updated_at", _session["updated_at"] );
			json_sessions.push_back(std::make_pair("", json_session));
		}

		json_app.add_child("sessions", json_sessions);
		json_app.add_child("permissions", json_permissions);

		root.add_child("data", json_app);

    	std::ostringstream buf; 
		write_json (buf, root, false);
		std::string json = buf.str(); 
		res.send(json, response::ok, "application/json");

	} catch(model_not_found_exception e) {

		string text = "{}";
		res.send(text, response::no_content, "application/json");    
	}

}


void app_controller::types(request req, response &res){ 

	ptree root;
	ptree json_app_types;

	app_type _app_type;
	_app_type.select()
			 .get();

	while(_app_type.has_next()) {
		ptree json_app_type;
		json_app_type.put("id", _app_type["id"]);
		json_app_type.put("name", _app_type["name"]);
		json_app_types.push_back(std::make_pair("", json_app_type));
	}

	root.put("page_records", _app_type.rows_count());
	root.put("total_records", _app_type.get_total_records());
	root.add_child("data", json_app_types);

	std::ostringstream buf; 
	write_json (buf, root, false);
	std::string json = buf.str(); 
	res.send(json, response::ok, "application/json");
}

void app_controller::permissions(request req, response &res){
	
	ptree root;
	ptree json_permissions;

	app_permission _permission;

	_permission.select()
			   .get();

	while(_permission.has_next()) {
		ptree json_permission;
		json_permission.put("id", _permission["id"]);
		json_permission.put("name", _permission["name"]);
		json_permissions.push_back(std::make_pair("", json_permission));
	}

	root.put("page_records", _permission.rows_count());
	root.put("total_records", _permission.get_total_records());
	root.add_child("data", json_permissions);

	std::ostringstream buf; 
	write_json (buf, root, false);
	std::string json = buf.str(); 
	res.send(json, response::ok, "application/json");
}


/**
*Store a record
*@param req: request
*@param res: response
*/
void app_controller::store(request req, response &res) {

	stringstream data_stream;

	data_stream << req.get_body();

	ptree root;
	ptree json_app;

	map <string, string> data;

	vector<string> permissions;

	try{

		read_json(data_stream, json_app);

	    data["name"]  = json_app.get<string>("name");
	    data["type_id"] = json_app.get<string>("type_id");
	    data["user_agent"] = json_app.get<string>("user_agent");
	    data["status"] = json_app.get<string>("status");
	    data["api_key"] =  app_service::generate_api_key();

	    if(json_app.count("permissions") > 0) {
		    BOOST_FOREACH(boost::property_tree::ptree::value_type &v, json_app.get_child("permissions"))
		    {
		    	permissions.push_back(v.second.data());
		    }
	    }

	    app::models::app _app;
    	_app.select()
    	    .where("name", "=", data["name"])
    	    .first_or_fail();

    	std::ostringstream buf; 
		write_json (buf, root, false);
		std::string json = buf.str(); 
		res.send(json, response::conflict, "application/json");

	} catch(model_not_found_exception e) {

		app::models::app _app;
		_app.first_after_create(data);

		if(permissions.size() > 0) {
			_app.attach("permissions", permissions);
		}

		json_app.put("id", _app["id"]);
		json_app.put("api_key",  data["api_key"]);
		json_app.put("updated_at",  _app["updated_at"]);
		json_app.put("created_at",  _app["created_at"]);

		root.add_child("data", json_app);

    	std::ostringstream buf; 
		write_json (buf, root, false);
		std::string json = buf.str(); 
		res.send(json, response::created, "application/json");

    } catch(...) {
    	std::ostringstream buf; 
		write_json (buf, root, false);
		std::string json = buf.str(); 
		res.send(json, response::bad_request, "application/json");    	
    }

}


/**
*Update a record
*@param req: request
*@param res: response
*/
void app_controller::update(request req, response &res) {

	bool reset_api_key = (req.get_header("reset-api-key") == "1");

	stringstream data_stream;
	data_stream << req.get_body();

	string id = req.get_uri_param("id");

	ptree json_app;

	map <string, string> data;
	vector<string> permissions;

	try{

		read_json(data_stream, json_app);

	    data["name"]  = json_app.get<string>("name");
	    data["type_id"] = json_app.get<string>("type_id");
	    data["user_agent"] = json_app.get<string>("user_agent");
	    data["status"] = json_app.get<string>("status");

	    if(reset_api_key) {
	    	data["api_key"] =  app_service::generate_api_key();
	    }

	    app::models::app _app;

		_app.select()
    	    .where("id", "=", id)
    	    .first_or_fail();

	    if(json_app.count("permissions") > 0) {

	    	_app.detach("permissions");
	    	
		    BOOST_FOREACH(boost::property_tree::ptree::value_type &v, json_app.get_child("permissions"))
		    {
		    	permissions.push_back(v.second.data());
		    }

			if(permissions.size() > 0) {
				_app.attach("permissions", permissions);
			}
	    }

	    _app.where("id", "=", id)
    		.update(data);

    	if(data["status"] != "1") {
	    	auth::force_session_destroy(id);
	    }

		std::string text = "Update success";
		res.send(text, response::ok, "application/json");

	} catch(model_not_found_exception e) {

		std::string text = "No content found";
		res.send(text, response::no_content, "application/json");


    } catch(...) {
		std::string text = "Bad Request";
		res.send(text, response::bad_request, "application/json");    	
    }

}


/**
*Delete a record
*@param req: request
*@param res: response
*/
void app_controller::destroy(request req, response &res) {

}



}//namespace controllers
}//namespace app