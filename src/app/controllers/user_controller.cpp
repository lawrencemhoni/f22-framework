/*
*user_controller.cpp
*This is an F22 controller
*lawrencetmhoni@gmail.com
*/

#include "user_controller.hpp"


namespace app {
namespace controllers {

	
/**
*Index records
*@param req: request
*@param res: response
*/
void user_controller::index(request req, response &res) {

	int items_per_page = string_to_int(
		req.get_get_input("items_per_page"), 15);

	int page_number = string_to_int(
		req.get_get_input("page"), 1);
	
	try {

		ptree root;
		ptree json_users;

		user _user;

		_user.select()
			 .with("accounts")
			 .paginate(items_per_page, page_number);

		while(_user.has_next()) {
			ptree json_user;

			json_user.put("id", _user["id"]);
			json_user.put("first_name", _user["first_name"]);
			json_user.put("last_name", _user["last_name"]);
			json_user.put("email", _user["email"]);
			json_user.put("username", _user["username"]);

			ptree json_accounts;
			user_account _account;

			_user.children("accounts", _account);

			while(_account.has_next()) {
				ptree json_account;
				json_account.put("id", _account["id"]);
				json_account.put("type_id", _account["type_id"]);
				json_account.put("organisation_id", _account["organisation_id"]);
				json_account.put("role", _account["role"]);
				json_accounts.push_back(make_pair("", json_account));
			}

			json_user.add_child("accounts", json_accounts);
			json_users.push_back(make_pair("", json_user));
		}

		root.add_child("data", json_users);
    	std::ostringstream buf; 
		write_json (buf, root, false);
		std::string json = buf.str(); 
		res.send(json, response::ok, "application/json");

	}catch(exception e) {



	}

}

/**
*View a record
*@param req: request
*@param res: response
*/
void user_controller::view(request req, response &res) {

	string id = req.get_uri_param("id");

	try{
		ptree root;
		ptree json_user;

		user _user;
		_user.find_detailed(id);

		json_user.put("id", _user["id"]);
		json_user.put("first_name", _user["first_name"]);
		json_user.put("last_name", _user["last_name"]);
		json_user.put("email", _user["email"]);
		json_user.put("username", _user["username"]);

		user_account _account;
		ptree json_accounts;

		_user.children("detailed_accounts", _account);

		while(_account.has_next()) {

			ptree json_account;
			json_account.put("id", _account["id"]);
			json_account.put("type_id", _account["type_id"]);
			json_account.put("user_id", _account["user_id"]);
			json_account.put("organisation_id", _account["organisation_id"]);
			json_account.put("status", _account["status"]);
			json_account.put("role", _account["role"]);

			account_permission _permission;
			ptree json_permissions;

			_account.children("permissions", _permission);

			while(_permission.has_next()) {
				ptree json_permission;
				json_permission.put("", _permission["id"] );
				json_permissions.push_back(std::make_pair("", json_permission));
			}

			json_account.add_child("permissions", json_permissions);
			json_accounts.push_back(std::make_pair("", json_account));
		}

		json_user.add_child("accounts", json_accounts);
	
		root.add_child("data", json_user);
    	std::ostringstream buf; 
		write_json (buf, root, false);
		std::string json = buf.str(); 
		res.send(json, response::ok, "application/json");

	}catch(model_not_found_exception e) {
		string text = "{}";
		res.send(text, response::no_content, "application/json");
	}catch(exception e) {
		cout << "Error " << endl;
		cout <<  e.what() << endl;
	}

}


/**
*Store a record
*@param req: request
*@param res: response
*/
void user_controller::store(request req, response &res) {

	stringstream data_stream;
	data_stream << req.get_body();

	ptree root;
	ptree json_user;

	user _user;
	map <string, string> data;

	try {

		read_json(data_stream, json_user);
		data["first_name"] = json_user.get<string>("first_name");
		data["last_name"]  = json_user.get<string>("last_name");
		data["email"]  	   = json_user.get<string>("email");
		data["username"]   = json_user.get<string>("username");
		data["password"]   = efficient::security::hash::make(json_user.get<string>("password"));

		_user.select()
			 .where("email", "=", data["email"])
			 .or_where("username", "=", data["username"])
			 .first_or_fail();

		std::ostringstream buf; 
		write_json (buf, root, false);
		std::string json = buf.str(); 
		res.send(json, response::conflict, "application/json");

	}catch(model_not_found_exception e) {

		_user.first_after_create(data);

		string user_id = _user["id"];
		json_user.put("id", user_id);

		if(json_user.count("accounts") > 0) { 

			BOOST_FOREACH(boost::property_tree::ptree::value_type &x, json_user.get_child("accounts"))
		    {
				user_account _account;
		    	ptree json_account = x.second;
		    
		    	map<string, string> account_data;
		    	account_data["user_id"] = user_id;
		    	account_data["type_id"] = json_account.get<string>("type_id");
		    	account_data["role"] = json_account.get<string>("role");
		    	account_data["organisation_id"] = json_account.get<string>("organisation_id");
		    	account_data["created_by"] = json_account.get<string>("created_by");
		    	account_data["status"] = json_account.get<string>("status");

		    	_account.first_after_create(account_data);

		    	vector<string> permissions;

		    	if(json_account.count("permissions") > 0) {

		    		BOOST_FOREACH(boost::property_tree::ptree::value_type &y, json_account.get_child("permissions"))
		    		{
		    			permissions.push_back(y.second.data());
		    		}

		    		_account.attach("permissions", permissions); 
				}
		    }
		}

		root.add_child("data", json_user);

		std::ostringstream buf; 
		write_json (buf, root, false);
		std::string json = buf.str(); 
		res.send(json, response::created, "application/json");

	}catch(exception const& e) {

		cout << "Error: " << endl;
		cout << e.what() << std::endl;
	}
}


/**
*Update a record
*@param req: request
*@param res: response
*/
void user_controller::update(request req, response &res) {

	stringstream data_stream;
	data_stream << req.get_body();

	ptree root;
	ptree json_user;

	user _user;
	map <string, string> data;

	string user_id = req.get_uri_param("id");

	try {

		read_json(data_stream, json_user);
		data["first_name"] = json_user.get<string>("first_name");
		data["last_name"]  = json_user.get<string>("last_name");
		data["email"]  	   = json_user.get<string>("email");
		data["username"]   = json_user.get<string>("username");

		if(json_user.count("password") && json_user.get<string>("password").length() > 0) {
			data["password"]   = efficient::security::hash::make(json_user.get<string>("password"));
		}

		_user.select()
			 .where("id", "=",  user_id)
			 .first_or_fail();

		_user.where("id", "=", user_id)
			 .update(data);

		
		json_user.put("id", user_id);

		if(json_user.count("accounts") > 0) { 
			_user.detach("accounts"); 
			 
			BOOST_FOREACH(boost::property_tree::ptree::value_type &x, json_user.get_child("accounts"))
		    {
				user_account _account;
		    	ptree json_account = x.second;
		    
		    	map<string, string> account_data;
		    	account_data["user_id"] = user_id;
		    	account_data["type_id"] = json_account.get<string>("type_id");
		    	account_data["role"] = json_account.get<string>("role");
		    	account_data["organisation_id"] = json_account.get<string>("organisation_id");
		    	account_data["created_by"] = json_account.get<string>("created_by");
		    	account_data["status"] = json_account.get<string>("status");

		    	_account.first_after_create(account_data);

		    	vector<string> permissions;

		    	if(json_account.count("permissions") > 0) {

		    		_account.detach("permissions"); 

		    		BOOST_FOREACH(boost::property_tree::ptree::value_type &y, json_account.get_child("permissions"))
		    		{
		    			permissions.push_back(y.second.data());
		    		}

		    		_account.attach("permissions", permissions); 
				}
		    }
		}

		root.add_child("data", json_user);

		std::ostringstream buf; 
		write_json (buf, root, false);
		std::string json = buf.str(); 
		res.send(json, response::ok, "application/json");


	}catch(model_not_found_exception e) {

		std::ostringstream buf; 
		write_json (buf, root, false);
		std::string json = buf.str(); 
		res.send(json, response::no_content, "application/json");

	}

}


/**
*Delete a record
*@param req: request
*@param res: response
*/
void user_controller::destroy(request req, response &res) {

}


void user_controller::account_types(request req, response &res) {

	account_type _account_type;

	_account_type.select(_argsv("id", "name", "description"))
				 .with("permissions")
				 .get();

	ptree root;
	ptree json_account_types;

	while(_account_type.has_next()) {

		cout << "Checking inside the loop" << endl;

		ptree json_account_type;

		json_account_type.put("id", _account_type["id"]);
		json_account_type.put("name", _account_type["name"]);
		json_account_type.put("description", _account_type["description"]);

		account_permission _permission;
		_account_type.children("account_permissions", _permission);

		ptree json_permissions;

		while(_permission.has_next()) {
			ptree json_permission;
			json_permission.put("id", _permission["id"]);
			json_permission.put("name", _permission["name"]);
			json_permissions.push_back(std::make_pair("", json_permission));
		}

		json_account_type.add_child("permissions", json_permissions);
		json_account_types.push_back(std::make_pair("", json_account_type));
	}

	root.put("total_records", _account_type.rows_count());
	root.add_child("data", json_account_types);
	
	std::ostringstream buf; 
	write_json (buf, root, false);
	std::string json = buf.str(); 
	res.send(json, response::ok, "application/json");
}


}//namespace controllers
}//namespace app