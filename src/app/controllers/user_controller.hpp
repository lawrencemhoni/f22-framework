/*
*user_controller.hpp
*This is an F22 controller
*lawrencetmhoni@gmail.com
*/

#ifndef APP_CONTROLLERS_USER_CONTROLLER_HPP
#define APP_CONTROLLERS_USER_CONTROLLER_HPP

#include <vector>
#include <map>
#include <sstream>
#include <exception>
#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "base_controller.hpp"

#include "../../core/generic/helpers.hpp"
#include "../../core/security/hash/hash.hpp"
#include "../../core/database/helpers.hpp"


#include "../models/user.hpp"
#include "../models/user_account.hpp"
#include "../models/account_type.hpp"
#include "../models/account_permission.hpp"

using namespace std;
using namespace efficient::http;
using namespace generic::helpers;
using namespace efficient::database::helpers;
using namespace app::models;


using app::models::account_type;
using app::models::account_permission;

using boost::property_tree::ptree;
using boost::property_tree::read_json;
using boost::property_tree::write_json;
using efficient::security::hash;

using efficient::database::exceptions::model_not_found_exception;


namespace app{
namespace controllers{ 


class user_controller {

public:

/**
*Index records
*@param req: request
*@param res: response
*/
static void index(request req, response &res);

/**
*View a record
*@param req: request
*@param res: response
*/
static void view(request req, response &res);

/**
*store a record
*@param req: request
*@param res: response
*/
static void store(request req, response &res);

/**
*Update a record
*@param req: request
*@param res: response
*/
static void update(request req, response &res);


static void account_types(request req, response &res);
	

/**
*Destroy a record
*@param req: request
*@param res: response
*/
static void destroy(request req, response &res);

}; //class user_controller


} //namespace controllers
} //namespace app


#endif //APP_CONTROLLERS_USER_CONTROLLER_HPP
