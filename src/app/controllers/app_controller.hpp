/*
*app_controller.hpp
*This is an F22 controller
*lawrencetmhoni@gmail.com
*/

#ifndef APP_CONTROLLERS_APP_CONTROLLER_HPP
#define APP_CONTROLLERS_APP_CONTROLLER_HPP

#include <vector>
#include <map>
#include <sstream>
#include <stdexcept>
#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "base_controller.hpp"
#include "../../core/generic/helpers.hpp"
#include "../../core/api/auth.hpp"
#include "../models/app_type.hpp"
#include "../models/app_permission.hpp"
#include "../models/app.hpp"
#include "../services/app_service.hpp"

using namespace std;
using namespace efficient::http;
using namespace efficient::api;
using namespace generic::helpers;
using namespace app::models;
using namespace app::services;

using boost::property_tree::ptree;
using boost::property_tree::read_json;
using boost::property_tree::write_json;

using efficient::database::exceptions::model_not_found_exception;


namespace app{
namespace controllers{ 


class app_controller {

public:

/**
*Index records
*@param req: request
*@param res: response
*/
static void index(request req, response &res);

/**
*View a record
*@param req: request
*@param res: response
*/
static void view(request req, response &res);



static void permissions(request req, response &res);


static void types(request req, response &res);


/**
*store a record
*@param req: request
*@param res: response
*/
static void store(request req, response &res);

/**
*Update a record
*@param req: request
*@param res: response
*/
static void update(request req, response &res);
	

/**
*Destroy a record
*@param req: request
*@param res: response
*/
static void destroy(request req, response &res);

}; //class app_controller


} //namespace controllers
} //namespace app


#endif //APP_CONTROLLERS_APP_CONTROLLER_HPP
