/**
*app_service.hpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#ifndef APP_SERVICES_APP_SERVICE_HPP
#define APP_SERVICES_APP_SERVICE_HPP

#include <iostream>

using namespace std;

namespace app{
namespace services{

	class app_service{

	public:
		app_service();

		static string generate_api_key();


	};

}//namespace services
}//namespace app


#endif //APP_SERVICES_APP_SERVICE_HPP