/**
*customer_service.hpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#ifndef APP_SERVICES_CUSTOMER_SERVICE_HPP
#define APP_SERVICES_CUSTOMER_SERVICE_HPP

#include <iostream>
#include "../../core/security/sweetheart/sweetheart.hpp"
#include "../../core/generic/helpers.hpp"
#include "../../core/generic/log.hpp"

using namespace std;

using generic::log;
using CryptoPP::Exception;

namespace app{
namespace services{

class customer_service{

public:
	customer_service();

	static bool decrypt_msisdn(string msisdn, string & decrypted_msisdn);

	static bool encrypt_msisdn(string msisdn, string & encrypted_msisdn);


};

}//namespace services
}//namespace app



#endif //APP_SERVICES_CUSTOMER_SERVICE_HPP