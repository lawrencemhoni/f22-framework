/**
*transaction_service.hpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#ifndef APP_SERVICES_TRANSACTION_SERVICE_HPP
#define APP_SERVICES_TRANSACTION_SERVICE_HPP

#include <iostream>

using namespace std;

namespace app{
namespace services{

	class transaction_service{

	public:
		transaction_service();
		
		static string generate_pass_code();

		static string generate_trans_code(string organisation_id, string app_id, string trans_id);

		static string generate_qr_code(string trans_code, string pass_code);




	};

}//namespace services
}//namespace app


#endif //APP_SERVICES_APP_SERVICE_HPP