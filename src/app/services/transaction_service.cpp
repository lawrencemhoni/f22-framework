/**
*transaction_service.cpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#include "transaction_service.hpp"

namespace app{
namespace services{

string transaction_service::generate_trans_code(
    string organisation_id, string app_id, string trans_id) {

    string trans_code = organisation_id + app_id + trans_id; 
    return trans_code;
}

string transaction_service::generate_pass_code() {

    string pass_code;

    unsigned int len = 12;
    char s[len];

    srand (time(NULL));

    const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    for (unsigned int i = 0; i < len; ++i) {
        s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    }

    s[len] = 0;
    return string(s);
}

string transaction_service::generate_qr_code(string trans_code, string pass_code) {

	string qr_code = trans_code + "-" + pass_code;
    return qr_code;
	
}




}//namespace services
}//namespace app