/**
*app_service.cpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#include "app_service.hpp"

namespace app{
namespace services{

string app_service::generate_api_key() {

	string api_key;

	unsigned int len = 15;
	char s[len];

	srand (time(NULL));

    const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    for (unsigned int i = 0; i < len; ++i) {
        s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    }

    s[len] = 0;

    api_key = string(s);
    return api_key;
}




}//namespace services
}//namespace app