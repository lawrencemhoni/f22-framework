/**
*customer_service.cpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#include "customer_service.hpp"

namespace app{
namespace services{

bool customer_service::decrypt_msisdn(string msisdn, string & decrypted_msisdn){

	try {

		efficient::security::sweetheart sh;
		sh.decode_decrypt(msisdn);
		decrypted_msisdn = sh.get_recovered_text();

		return true;

	} catch (CryptoPP::Exception e) {

		decrypted_msisdn  = string("");
		string msg = "MSISDN " + msisdn + " Could not be decrypted";
		log::error(msg);

		e.what();
	}

	return false;
}


bool customer_service::encrypt_msisdn(string msisdn, string & encrypted_msisdn) {

	try {
		efficient::security::sweetheart sh;
		sh.encrypt(msisdn);
		encrypted_msisdn = sh.get_encoded_cipher_text();

		return  true;

	} catch (CryptoPP::Exception e) {

		encrypted_msisdn  = string("");
		string msg = "MSISDN " + msisdn + " Could not be encrypted";
		log::error(msg);

		e.what();
	}

	return false;
}

}//namespace services
}//namespace app
