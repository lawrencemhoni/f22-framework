/*
*app.cpp
*copyright 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#include "app.hpp"

namespace app {
namespace models{


app::app(){

	set_table_name("apps");
	set_primary_key("id");

	add_belongs_to_many("permissions", new app_permission(), 
		"app_permission", "app_id", "permission_id");
}


app & app::find_detailed(string id) {

	app_session * x = new app_session();

	x->limit("5");
	x->order_by("id", "DESC");

	add_has_many("recent_sessions", x,  "app_id");
	select();
	where("id", "=", id);
	with("recent_sessions");
	with("permissions");
	first_or_fail();

	return *this;
}





}//namespace models
}//namespace app