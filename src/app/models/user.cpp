/*
*user.cpp
*copyright 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#include "user.hpp"

namespace app {
namespace models{


user::user(){

	set_table_name("users");
	set_primary_key("id");

	add_has_many("accounts", new user_account(), "user_id");
}


user & user::find_detailed(string id) {

	user_account * _account = new user_account();

	_account->select();
	_account->with("permissions");
	
	add_has_many("detailed_accounts", _account, "user_id" );

	select();
	where("id", "=", id);
	with("detailed_accounts");
	first_or_fail();
	return *this;
}





}//namespace models
}//namespace app