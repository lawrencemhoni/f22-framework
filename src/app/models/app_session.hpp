/*app_session.hpp
*copyright (c) 2016 - 2020 Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/

#ifndef APP_MODELS_APP_SESSION_HPP
#define APP_MODELS_APP_SESSION_HPP

#include "../../core/database/model.hpp"

using namespace efficient::database;


namespace app {
namespace models{

class app_session  : public  model {

public :
	app_session();

};

}//namespace models
}//app

#endif // APP_SESSION