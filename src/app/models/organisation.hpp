/*organisation.hpp
*copyright (c) 2016 - 2020 Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/

#ifndef APP_MODELS_ORGANISATION_HPP
#define APP_MODELS_ORGANISATION_HPP

#include "../../core/database/model.hpp"

using namespace efficient::database;


namespace app {
namespace models{

class organisation  : public  model {

public :
	organisation();

};

}//namespace models
}//app

#endif // ORGANISATION