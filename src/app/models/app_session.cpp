/*
*app_session.cpp
*copyright 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#include "app_session.hpp"

namespace app {
namespace models{


app_session::app_session(){

	set_table_name("app_sessions");
	set_primary_key("id");
}





}//namespace models
}//namespace app