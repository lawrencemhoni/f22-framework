/*
*app_type.cpp
*copyright 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#include "app_type.hpp"

namespace app {
namespace models{


app_type::app_type(){

	set_table_name("app_types");
	set_primary_key("id");
}





}//namespace models
}//namespace app