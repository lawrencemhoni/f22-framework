/*
*account_type.cpp
*copyright 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#include "account_type.hpp"

namespace app {
namespace models{


account_type::account_type(){

	set_table_name("account_types");
	set_primary_key("id");

	add_belongs_to_many("account_permissions", new account_permission(), 
		"account_type_permission", "account_type_id", "permission_id");
}

account_type & account_type::detailed() {
	return *this;
}



}//namespace models
}//namespace app