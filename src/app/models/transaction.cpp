/*
*transaction.cpp
*copyright 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#include "transaction.hpp"

namespace app {
namespace models{


transaction::transaction(){

	set_table_name("transactions");
	set_primary_key("id");

	add_has_many("products", new transaction_product(), "transaction_id");
}





}//namespace modssels
}//namespace app