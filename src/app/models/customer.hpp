/*customer.hpp
*copyright (c) 2016 - 2020 Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/

#ifndef APP_MODELS_CUSTOMER_HPP
#define APP_MODELS_CUSTOMER_HPP

#include "../../core/database/model.hpp"
#include "customer_mobile.hpp"

using namespace efficient::database;


namespace app {
namespace models{

class customer  : public  model {

public :
	customer();

};

}//namespace models
}//app

#endif