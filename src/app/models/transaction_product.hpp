/*transaction_product.hpp
*copyright (c) 2016 - 2020 Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/

#ifndef APP_MODELS_TRANSACTION_PRODUCT_HPP
#define APP_MODELS_TRANSACTION_PRODUCT_HPP

#include "../../core/database/model.hpp"

using namespace efficient::database;


namespace app {
namespace models{

class transaction_product  : public  model {

public :
	transaction_product();

};

}//namespace models
}//app

#endif // TRANSACTION_PRODUCT