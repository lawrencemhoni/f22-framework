/*account_type.hpp
*copyright (c) 2016 - 2020 Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/

#ifndef APP_MODELS_ACCOUNT_TYPE_HPP
#define APP_MODELS_ACCOUNT_TYPE_HPP

#include "../../core/database/model.hpp"
#include "./account_permission.hpp"

using namespace efficient::database;


namespace app {
namespace models{

class account_type  : public  model {

public :
	account_type();

	account_type & detailed();
};

}//namespace models
}//app

#endif // ACCOUNT_TYPE