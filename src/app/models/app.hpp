/*app.hpp
*copyright (c) 2016 - 2020 Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/

#ifndef APP_MODELS_APP_HPP
#define APP_MODELS_APP_HPP

#include "../../core/database/model.hpp"
#include "app_permission.hpp"
#include "app_session.hpp"


using namespace efficient::database;


namespace app {
namespace models{

class app  : public  model {

public :
	app();

	app & find_detailed(string id);

};

}//namespace models
}//app

#endif // APP