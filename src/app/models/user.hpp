/*user.hpp
*copyright (c) 2016 - 2020 Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/

#ifndef APP_MODELS_USER_HPP
#define APP_MODELS_USER_HPP

#include "../../core/database/model.hpp"
#include "../../core/generic/helpers.hpp"
#include "user_account.hpp"

using namespace efficient::database;
using namespace generic::helpers;


namespace app {
namespace models{

class user  : public  model {

public :
	user();

	user & find_detailed(string id);

};

}//namespace models
}//app

#endif // USER