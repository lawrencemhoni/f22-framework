/*
*customer.cpp
*copyright 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#include "customer.hpp"

namespace app {
namespace models{


customer::customer(){

	set_table_name("customers");
	set_primary_key("id");

	add_has_many("mobiles", new customer_mobile(), "customer_id");
}





}//namespace models
}//namespace app