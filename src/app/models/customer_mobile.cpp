/*
*mobile.cpp
*copyright (c) 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#include "customer_mobile.hpp"

namespace app {
namespace models {

customer_mobile::customer_mobile() {
	set_table_name("customer_mobiles");
	set_primary_key("id");
}




} //namespace app
} //namespace models