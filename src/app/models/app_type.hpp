/*app_type.hpp
*copyright (c) 2016 - 2020 Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/

#ifndef APP_MODELS_APP_TYPE_HPP
#define APP_MODELS_APP_TYPE_HPP

#include "../../core/database/model.hpp"

using namespace efficient::database;


namespace app {
namespace models{

class app_type  : public  model {

public :
	app_type();

};

}//namespace models
}//app

#endif // APP_TYPE