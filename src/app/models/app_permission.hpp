/*app_permission.hpp
*copyright (c) 2016 - 2020 Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/

#ifndef APP_MODELS_APP_PERMISSION_HPP
#define APP_MODELS_APP_PERMISSION_HPP

#include "../../core/database/model.hpp"

using namespace efficient::database;


namespace app {
namespace models{

class app_permission  : public  model {

public :
	app_permission();

};

}//namespace models
}//app

#endif // APP_PERMISSION