/*transaction.hpp
*copyright (c) 2016 - 2020 Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/

#ifndef APP_MODELS_TRANSACTION_HPP
#define APP_MODELS_TRANSACTION_HPP

#include "../../core/database/model.hpp"
#include "transaction_product.hpp"

using namespace efficient::database;


namespace app {
namespace models{

class transaction  : public  model {

public :
	transaction();

};

}//namespace models
}//app

#endif // TRANSACTION