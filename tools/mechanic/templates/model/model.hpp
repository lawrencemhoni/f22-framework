/*[%model_name].hpp
*copyright (c) 2016 - 2020 Lawrence T Mhoni
*lawrencemhoni@gmail.com
*/

#ifndef APP_MODELS_[%defining_name]_HPP
#define APP_MODELS_[%defining_name]_HPP

#include "../../core/database/model.hpp"

using namespace efficient::database;


namespace app {
namespace models{

class [%model_name]  : public  model {

public :
	[%model_name]();

};

}//namespace models
}//app

#endif // [%defining_name]