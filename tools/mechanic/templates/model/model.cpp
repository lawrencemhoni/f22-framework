/*
*[%model_name].cpp
*copyright 2016-2020 Lawrence T Mhoni
*lawrencetmhoni@gmail.com
*/

#include "[%model_name].hpp"

namespace app {
namespace models{


[%model_name]::[%model_name](){

	set_table_name("");
	set_primary_key("id");
}





}//namespace models
}//namespace app