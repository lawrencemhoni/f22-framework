/*
*[%controller_name].cpp
*This is an F22 controller
*lawrencetmhoni@gmail.com
*/

#include "[%controller_name].hpp"


namespace app {
namespace controllers {

	
/**
*Index records
*@param req: request
*@param res: response
*/
void [%controller_name]::index(request req, response &res) {

}

/**
*View a record
*@param req: request
*@param res: response
*/
void [%controller_name]::view(request req, response &res) {

}


/**
*Store a record
*@param req: request
*@param res: response
*/
void [%controller_name]::store(request req, response &res) {

}


/**
*Update a record
*@param req: request
*@param res: response
*/
void [%controller_name]::update(request req, response &res) {

}


/**
*Delete a record
*@param req: request
*@param res: response
*/
void [%controller_name]::destroy(request req, response &res) {

}



}//namespace controllers
}//namespace app