/*
*[%controller_name].hpp
*This is an F22 controller
*lawrencetmhoni@gmail.com
*/

#ifndef APP_CONTROLLERS_[%defining_name]_HPP
#define APP_CONTROLLERS_[%defining_name]_HPP

#include "base_controller.hpp"

using namespace std;
using namespace efficient::http;


namespace app{
namespace controllers{ 


class [%controller_name] {

public:

/**
*Index records
*@param req: request
*@param res: response
*/
static void index(request req, response &res);

/**
*View a record
*@param req: request
*@param res: response
*/
static void view(request req, response &res);

/**
*store a record
*@param req: request
*@param res: response
*/
static void store(request req, response &res);

/**
*Update a record
*@param req: request
*@param res: response
*/
static void update(request req, response &res);
	

/**
*Destroy a record
*@param req: request
*@param res: response
*/
static void destroy(request req, response &res);

}; //class [%controller_name]


} //namespace controllers
} //namespace app


#endif //APP_CONTROLLERS_[%defining_name]_HPP
